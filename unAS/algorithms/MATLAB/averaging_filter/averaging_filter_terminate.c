/*
 * averaging_filter_terminate.c
 *
 * Code generation for function 'averaging_filter_terminate'
 *
 * C source code generated on: Fri Jan  4 10:57:45 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "averaging_filter.h"
#include "averaging_filter_terminate.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void averaging_filter_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (averaging_filter_terminate.c) */
