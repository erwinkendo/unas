/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"




static int16_t pinint[4] = {0,0,0,0};
static int16_t count =0;


/*
 * PWM configuration structure.
 * Channels:    All channels enabled.
 */
static PWMConfig pwmcfg = {
  10000,                                  /* 10kHz PWM clock frequency.   */
  100,                                    /* Initial PWM period 1S.       */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  0,
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Initializes the PWM driver 4, routes the TIM4 outputs to the board LEDs.
   */
  pwmStart(&PWMD4, &pwmcfg);
  palSetPadMode(GPIOD, 12, PAL_MODE_ALTERNATE(2));      /* Green.   */
  palSetPadMode(GPIOD, 13, PAL_MODE_ALTERNATE(2));      /* Orange.  */
  palSetPadMode(GPIOD, 14, PAL_MODE_ALTERNATE(2));      /* Red.     */
  palSetPadMode(GPIOD, 15, PAL_MODE_ALTERNATE(2));      /* Blue.    */
  

  
  /*
   * Normal main() thread activity, in this demo it changes the LEDS intensienables and disables the
   */
  while (TRUE) {
    


for (count=1000;count<=10000;count=count+500){
	

	pinint[0] = PWM_PERCENTAGE_TO_WIDTH(&PWMD4, count);
	pinint[1] = pinint[0];
	pinint[2] = pinint[0];
	pinint[3] = pinint[0];
	pwmEnableChannel(&PWMD4, 0, pinint[0]);
   	pwmEnableChannel(&PWMD4, 1, pinint[1]);
   	pwmEnableChannel(&PWMD4, 2, pinint[2]);
 	pwmEnableChannel(&PWMD4, 3, pinint[3]);
	chThdSleepMilliseconds(50);
		
}
for(count=10000;count>=500;count=count-500){
	pinint[0] = PWM_PERCENTAGE_TO_WIDTH(&PWMD4, count);
	pinint[1] = pinint[0];
	pinint[2] = pinint[0];
	pinint[3] = pinint[0];
	pwmEnableChannel(&PWMD4, 0, pinint[0]);
  	pwmEnableChannel(&PWMD4, 1, pinint[1]);
    	pwmEnableChannel(&PWMD4, 2, pinint[2]);
    	pwmEnableChannel(&PWMD4, 3, pinint[3]);
	chThdSleepMilliseconds(50);
	
}



	
  }
}
