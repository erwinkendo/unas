/*
 * averaging_filter_data.h
 *
 * Code generation for function 'averaging_filter_data'
 *
 * C source code generated on: Fri Jan  4 10:57:45 2013
 *
 */

#ifndef __AVERAGING_FILTER_DATA_H__
#define __AVERAGING_FILTER_DATA_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "averaging_filter_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */
extern real_T buffer[16];

/* Variable Definitions */

/* Function Declarations */
#endif
/* End of code generation (averaging_filter_data.h) */
