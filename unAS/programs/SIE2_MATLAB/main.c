/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <math.h>
#include "ch.h"
#include "hal.h"
#include "chprintf.h"
// #include "averaging_filter.h"
// #include "kalmanfilter.h"
// #include "kalmanfilter_initialize.h"
#include "unas_kalman_V2.h"
#include "unas_kalman_V2_initialize.h"


#define ADC_GRP1_NUM_CHANNELS   6
#define ADC_GRP1_BUF_DEPTH      1

#define ADC_GRP2_NUM_CHANNELS   1
#define ADC_GRP2_BUF_DEPTH      1

static uint32_t time;
static uint32_t delay = 1000;
// static uint8_t tlcounter = 0;

static float gx;
static float gy;
static float gz;
static float ax;
static float ay;
static float az;
static float mx;
static float my;
static float mz;

/* SPI IMU values. */
static uint16_t bursttxbuf[14] = {0x4200, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
static uint16_t burstrxbuf[14] = {0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
// static uint16_t imutxbuf[1] = {0x5600};
// static uint16_t imurxbuf[1] = {0x0000};

/* ADC measurements array. */
// static adcsample_t samples1[ADC_GRP1_NUM_CHANNELS * ADC_GRP1_BUF_DEPTH];
// static adcsample_t samples2[ADC_GRP2_NUM_CHANNELS * ADC_GRP2_BUF_DEPTH];

// static real_T matlab_in[1024];
// static real_T matlab_out[1024];
// static real_T kalman_in[2];
// static real_T kalman_out[2];

static real_T kalman_v2_in[6];
static real_T kalman_v2_out[3];

static TimeMeasurement measure1, measure2;

/*
 * ADC conversion group for Servo Positions.
 * Mode:        Linear buffer, 1 samples of 6 channel, SW triggered.
 * Channels:    IN10, IN11, IN12, IN13, IN14, IN15.
 */
static const ADCConversionGroup adcgrpcfg1 = {
  FALSE,
  ADC_GRP1_NUM_CHANNELS,
  NULL,
  NULL,
  0, 0,                         /* CR1, CR2 */
  ADC_SMPR1_SMP_AN10(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN11(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN12(ADC_SAMPLE_1P5) | 
  ADC_SMPR1_SMP_AN13(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN14(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN15(ADC_SAMPLE_1P5), 
  0,
  ADC_SQR1_NUM_CH(ADC_GRP1_NUM_CHANNELS),
  0,                            /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN10) | ADC_SQR3_SQ2_N(ADC_CHANNEL_IN11) | ADC_SQR3_SQ3_N(ADC_CHANNEL_IN12) | 
  ADC_SQR3_SQ4_N(ADC_CHANNEL_IN13) | ADC_SQR3_SQ5_N(ADC_CHANNEL_IN14) | ADC_SQR3_SQ6_N(ADC_CHANNEL_IN15) 
};

/*
 * ADC conversion group for Preasure Sensor.
 * Mode:        Linear buffer, 1 samples of 1 channel, SW triggered.
 * Channels:    IN9.
 */
static const ADCConversionGroup adcgrpcfg2 = {
  FALSE,
  ADC_GRP2_NUM_CHANNELS,
  NULL,
  NULL,
  0, 0,                         /* CR1, CR2 */
  0, 
  ADC_SMPR2_SMP_AN9(ADC_SAMPLE_1P5) ,
  ADC_SQR1_NUM_CH(ADC_GRP2_NUM_CHANNELS),
  0,                            /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN9)
};

/*
 * IMU configuration (1.312500MHz, CPHA=1, CPOL=1, MSb first, DFF=1(16-bit data frame format)).
 */
static const SPIConfig ps_spicfg = {
  NULL,
  GPIOB,
  12,
  SPI_CR1_BR_2 | SPI_CR1_BR_0 | SPI_CR1_CPHA | SPI_CR1_CPOL | SPI_CR1_DFF
};

/*
 * SERIAL Device configuration for RF.
 * BitRate:     57600.
 */
static SerialConfig SERIALrfcfg = {
  57600,
  0,
  USART_CR2_STOP1_BITS | USART_CR2_LINEN,
  0
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 9, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOA, 10, PAL_MODE_INPUT);				/* Rx.     */
  
//   uartStart(&UARTD3,&UARTpcfgrf);
  sdStart(&SD3, &SERIALrfcfg);
  palSetPadMode(GPIOC, 10, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOC, 11, PAL_MODE_INPUT);				/* Rx.     */
  
  /* ADC Group of Servo Measurements*/
  palSetGroupMode(GPIOC, PAL_PORT_BIT(0) | PAL_PORT_BIT(1) | PAL_PORT_BIT(2) | 
			 PAL_PORT_BIT(3) | PAL_PORT_BIT(4) | PAL_PORT_BIT(5),
                  0, PAL_MODE_INPUT_ANALOG);
  /* Preasure Sensor Measure Pin. */
  palSetPadMode(GPIOB, 1, PAL_MODE_INPUT_ANALOG);
  adcStart(&ADCD1, NULL);
    
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
    palSetPadMode(GPIOD, GPIOD_LED, PAL_MODE_OUTPUT_PUSHPULL);

//     uint32_t k =0;
//   for (k = 0; k < 1024; k++) {
//     matlab_in[k] = k;
//   }

//   kalmanfilter_initialize();
  unas_kalman_V2_initialize();
  
  tmObjectInit(&measure1);
  tmObjectInit(&measure2);

  /* IMU SPI Pins. */
  palSetPadMode(GPIOB, 13, PAL_MODE_STM32_ALTERNATE_PUSHPULL);     /* SCK. */
  palSetPadMode(GPIOB, 14, PAL_MODE_STM32_ALTERNATE_PUSHPULL);     /* MISO.*/
  palSetPadMode(GPIOB, 15, PAL_MODE_STM32_ALTERNATE_PUSHPULL);     /* MOSI.*/
  palSetPadMode(GPIOB, 12, PAL_MODE_OUTPUT_PUSHPULL);		   /* NS. */
  palSetPad(GPIOB, 12);
  spiAcquireBus(&SPID2);              /* Acquire ownership of the bus.    */
  spiStart(&SPID2, &ps_spicfg);       /* Setup transfer parameters.       */
  spiSelect(&SPID2);                  /* Slave Select assertion.          */
    
  while (TRUE) {
        
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
//     tmStartMeasurement(&measure1);
//     adcConvert(&ADCD1, &adcgrpcfg1, samples1, ADC_GRP1_BUF_DEPTH);
//     adcConvert(&ADCD1, &adcgrpcfg2, samples2, ADC_GRP2_BUF_DEPTH);
    
//     kalman_in[0] = samples1[0];
//     kalman_in[1] = samples1[2];
//     kalman_in[0] = 100;
//     kalman_in[1] = 100;  
//     kalman_out[0] = 100;
//     kalman_out[1] = 100; 
    
//     x_est[0] = 1;
//     x_est[1] = 1;
//     x_est[2] = 1;
    
//     chThdSleepMilliseconds(1000);
    
//     unas_kalman_V2_initialize();
    
//     chThdSleepMilliseconds(1000);
// 
//     // kalman_v2 before input    
//     chprintf((BaseSequentialStream *)&SD1, "%f \n", kalman_v2_out[0]);
//     chThdSleepMilliseconds(1000);
//     
//     kalman_v2_in[0] = 0.15;
//     kalman_v2_in[1] = 0.3;
//     kalman_v2_in[2] = -0.05;
//     kalman_v2_in[3] = -43.329;
//     kalman_v2_in[4] = 26.664;
//     kalman_v2_in[5] = -1003.233;
//     
//     unas_kalman_V2(kalman_v2_in, kalman_v2_out);
//     chprintf((BaseSequentialStream *)&SD1, "%f \n", kalman_v2_out[0]);
// //     chThdSleepMilliseconds(1000);
//     // kalman_v2 iteration 0    
// //     chprintf((BaseSequentialStream *)&SD1, "Iteration 1: \n\rIN: %f %f %f %f %f %f \n\rOUT: %f %f %f \n\r", 
// // 	  kalman_v2_in[0], kalman_v2_in[1], kalman_v2_in[2], kalman_v2_in[3], kalman_v2_in[4], kalman_v2_in[5], 
// // 	  kalman_v2_out[0], kalman_v2_out[1], kalman_v2_out[2]);
//     
//     chThdSleepMilliseconds(1000);
// 
//     kalman_v2_in[0] = 0.1;
//     kalman_v2_in[1] = 0.25;
//     kalman_v2_in[2] = 0.05;
//     kalman_v2_in[3] = -39.996;
//     kalman_v2_in[4] = 26.664;
//     kalman_v2_in[5] = -1003.233;
//     
//     unas_kalman_V2(kalman_v2_in, kalman_v2_out);
//     chprintf((BaseSequentialStream *)&SD1, "%f \n", kalman_v2_out[0]);
// 
// //     // kalman_v2 iteration 1    
// //     chprintf((BaseSequentialStream *)&SD1, "Iteration 2: \n\rIN: %f %f %f %f %f %f \n\rOUT: %f %f %f \n\r", 
// // 	  kalman_v2_in[0], kalman_v2_in[1], kalman_v2_in[2], kalman_v2_in[3], kalman_v2_in[4], kalman_v2_in[5], 
// // 	  kalman_v2_out[0], kalman_v2_out[1], kalman_v2_out[2]);
//     chThdSleepMilliseconds(1000);
// //           
//     kalman_v2_in[0] = 0.1;
//     kalman_v2_in[1] = 0.25;
//     kalman_v2_in[2] = 0.05;
//     kalman_v2_in[3] = -39.996;
//     kalman_v2_in[4] = 26.664;
//     kalman_v2_in[5] = -1003.233;
// //     
//     unas_kalman_V2(kalman_v2_in, kalman_v2_out);
//     chprintf((BaseSequentialStream *)&SD1, "%f \n", kalman_v2_out[0]);
//     
// //     // kalman_v2 iteration 2    
// //     chprintf((BaseSequentialStream *)&SD1, "Iteration 3: \n\rIN: %f %f %f %f %f %f \n\rOUT: %f %f %f \n\r", 
// // 	  kalman_v2_in[0], kalman_v2_in[1], kalman_v2_in[2], kalman_v2_in[3], kalman_v2_in[4], kalman_v2_in[5], 
// // 	  kalman_v2_out[0], kalman_v2_out[1], kalman_v2_out[2]);
//     chThdSleepMilliseconds(1000);
// //     
//     kalman_v2_in[0] = 0.1;
//     kalman_v2_in[1] = 0.25;
//     kalman_v2_in[2] = 0.05;
//     kalman_v2_in[3] = -39.996;
//     kalman_v2_in[4] = 23.331;
//     kalman_v2_in[5] = -1003.233;
//     
//     unas_kalman_V2(kalman_v2_in, kalman_v2_out);
//     chprintf((BaseSequentialStream *)&SD1, "%f \n", kalman_v2_out[0]);
//     
// //     // kalman_v2 iteration 3    
// //     chprintf((BaseSequentialStream *)&SD1, "Iteration 4: \n\rIN: %f %f %f %f %f %f \n\rOUT: %f %f %f \n\r", 
// // 	  kalman_v2_in[0], kalman_v2_in[1], kalman_v2_in[2], kalman_v2_in[3], kalman_v2_in[4], kalman_v2_in[5], 
// // 	  kalman_v2_out[0], kalman_v2_out[1], kalman_v2_out[2]);
//     chThdSleepMilliseconds(1000);
// //     
//     kalman_v2_in[0] = 0.15;
//     kalman_v2_in[1] = 0.25;
//     kalman_v2_in[2] = 0.05;
//     kalman_v2_in[3] = -39.663;
//     kalman_v2_in[4] = 23.331;
//     kalman_v2_in[5] = -1003.233;
// //     
//     unas_kalman_V2(kalman_v2_in, kalman_v2_out);
//     chprintf((BaseSequentialStream *)&SD1, "%f \n", kalman_v2_out[0]);
//     
// //     // kalman_v2 iteration 4    
// //     chprintf((BaseSequentialStream *)&SD1, "Iteration 5: \n\rIN: %f %f %f %f %f %f \n\rOUT: %f %f %f \n\r", 
// // 	  kalman_v2_in[0], kalman_v2_in[1], kalman_v2_in[2], kalman_v2_in[3], kalman_v2_in[4], kalman_v2_in[5], 
// // 	  kalman_v2_out[0], kalman_v2_out[1], kalman_v2_out[2]);
//     chThdSleepMilliseconds(1000);
//     
    
    spiSelect(&SPID2);                  /* Slave Select assertion.          */
    spiReceive(&SPID2, 14, burstrxbuf);          /* Atomic transfer operations.      */
    spiSend(&SPID2, 1, bursttxbuf);          /* Atomic transfer operations.      */
    spiUnselect(&SPID2);                  /* Slave Select assertion.          */
    
    gx = (burstrxbuf[1]&0x2000)>1?((int16_t)((~(burstrxbuf[1]|0xC000))+1)*-0.05):((burstrxbuf[1]&0x3FFF)*0.05);
    gy = (burstrxbuf[2]&0x2000)>1?((int16_t)((~(burstrxbuf[2]|0xC000))+1)*-0.05):((burstrxbuf[2]&0x3FFF)*0.05);
    gz = (burstrxbuf[3]&0x2000)>1?((int16_t)((~(burstrxbuf[3]|0xC000))+1)*-0.05):((burstrxbuf[3]&0x3FFF)*0.05);
    ax = (burstrxbuf[4]&0x2000)>1?((int16_t)((~(burstrxbuf[4]|0xC000))+1)*-3.333):((burstrxbuf[4]&0x3FFF)*3.333);
    ay = (burstrxbuf[5]&0x2000)>1?((int16_t)((~(burstrxbuf[5]|0xC000))+1)*-3.333):((burstrxbuf[5]&0x3FFF)*3.333);
    az = (burstrxbuf[6]&0x2000)>1?((int16_t)((~(burstrxbuf[6]|0xC000))+1)*-3.333):((burstrxbuf[6]&0x3FFF)*3.333);
    mx = (burstrxbuf[7]&0x2000)>1?((int16_t)((~(burstrxbuf[7]|0xC000))+1)*-0.5):((burstrxbuf[7]&0x3FFF)*0.5);
    my = (burstrxbuf[8]&0x2000)>1?((int16_t)((~(burstrxbuf[8]|0xC000))+1)*-0.5):((burstrxbuf[8]&0x3FFF)*0.5);
    mz = (burstrxbuf[9]&0x2000)>1?((int16_t)((~(burstrxbuf[9]|0xC000))+1)*-0.5):((burstrxbuf[9]&0x3FFF)*0.5);

    kalman_v2_in[0] = gx;
    kalman_v2_in[1] = gy;
    kalman_v2_in[2] = gz;
    kalman_v2_in[3] = ax;
    kalman_v2_in[4] = ay;
    kalman_v2_in[5] = az;
    
    unas_kalman_V2(kalman_v2_in, kalman_v2_out);
    
        /* Test Dump Stream 2*/
//     chprintf((BaseSequentialStream *)&SD1, "%f %f %f %f %f %f %f %f %f\r\n", 
// 	  gx,gy,gz,
// 	  ax,ay,az,
// 	  mx,my,mz);
//     chprintf((BaseSequentialStream *)&SD1, "S %X %X %X %X %X %X %X %X %X %X %X %X %X %X %X %X %X %f\r\n", 
// 	  burstrxbuf[0], burstrxbuf[1], burstrxbuf[2], burstrxbuf[3], 
// 	  burstrxbuf[4], burstrxbuf[5], burstrxbuf[6], burstrxbuf[7], 
// 	  burstrxbuf[8], burstrxbuf[9], burstrxbuf[10], burstrxbuf[11], 
// 	  burstrxbuf[12], burstrxbuf[13],
// 	  (uint16_t)8190, (uint16_t)8190|0xC000,(int16_t)(~(8190|0xC000)), gx);
//     chprintf((BaseSequentialStream *)&SD1, "S %X %X %X %d %f\r\n",   
//       (uint16_t)0X3FFE, (uint16_t)0X3FFE|0xC000,(uint16_t)(~(0X3FFE|0xC000)), (int16_t)((~(0X3FFE|0xC000))+1)*-1), gx;
    
//     tmStartMeasurement(&measure2);
//     averaging_filter(matlab_in, matlab_out);  
//     kalmanfilter(kalman_in, kalman_out);
//     unas_kalman_V2(kalman_v2_in, kalman_v2_out);
//     tmStopMeasurement(&measure2);
//     matlab_in[0] = samples1[0];    
//     tlcounter +=1;
//     
//     if(tlcounter == 50) {
//       /* Telemetry Output. */
//       chprintf((BaseSequentialStream *)&SD3, "Sigo vivo %u \r\n", time);
//       tlcounter = 0;
//     }
    
    chThdSleepUntil(time);
//     chprintf((BaseSequentialStream *)&SD1, "%U %U %U \n", 
// 	  measure2.last, measure2.best, measure2.worst,time);
    chprintf((BaseSequentialStream *)&SD1, "%X \r\n",delay);
    palTogglePad(GPIOD, GPIOD_LED);

//     tmStopMeasurement(&measure1);
    /* Test Dump Stream 2*/   
//     chprintf((BaseSequentialStream *)&SD1, "S %f %f %f %f %f \rMeasure1 %U %U %U \rMeasure2 %U %U %U %u \r", 
// 	  matlab_out[0], kalman_in[0], kalman_in[1], kalman_out[0], kalman_out[1], measure1.last, measure1.best, measure1.worst, measure2.last, measure2.best, measure2.worst,time);
//     chprintf((BaseSequentialStream *)&SD1, "S %f %f %f %f %f %f\r", 
// 	  x_est[0], x_est[0], x_est[0], x_est[0], x_est[0], x_est[0]);
    
//     palTogglePad(GPIOD, GPIOD_LED);

  }
}
