
/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "mavlink.h"



static mavlink_message_t msginput;
static mavlink_status_t statusinput;
static uint8_t ledinput[5];

void uartfun(UARTDriver *uartp, uint16_t c) {
  (void) uartp;

  /* Initial GPS Decoding condition, no previous operation done. */
  if(mavlink_parse_char(MAVLINK_COMM_0, c, &msginput, &statusinput)) {
    // Handle message
    switch(msginput.msgid) {
      case MAVLINK_MSG_ID_HEARTBEAT:
	palTogglePad(GPIOD, 14);
	break;
      case MAVLINK_MSG_ID_ORDER_TEST:
	palTogglePad(GPIOD, 13);
	mavlink_msg_order_test_get_led(&msginput, &ledinput);
	chprintf((BaseSequentialStream *)&SD4, "%U %U %U %U %U \n\r", 
		 ledinput[0], ledinput[1], ledinput[2], ledinput[3], ledinput[4]);
	break;
      default:
	break;
    }
  }
}

static const UARTConfig UARTpcfggps = {
  NULL,
  NULL,
  NULL,
  uartfun,
  NULL,
  /* HW dependent part.*/
  115200,
  0,
  0,
  0
};


/*
 * Application entry point.
 */
int main(void) {

  uint32_t time;
  uint32_t delay = 1000;
  uint8_t tlcounter = 0;
  
  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USART2.
   */
  uartStart(&UARTD2,&UARTpcfggps);
//   uartStart(&UARTD2,&lea6hUARTpcfg);
  
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));

  /*
   * Activates the serial driver 4 using the driver default configuration.
   * PA0(TX) are routed to UART4.
   */
  sdStart(&SD4, NULL);
  palSetPadMode(GPIOA, 0, PAL_MODE_ALTERNATE(8));


  chThdSleepMilliseconds(500);
    
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state, when the button is
   * pressed the test procedure is launched with output on the serial
   * driver 2.
   */

  mavlink_system_t mavlink_system;
 
  mavlink_system.sysid = 1;                   ///< ID 1 for this airplane
  mavlink_system.compid = MAV_COMP_ID_SYSTEM_CONTROL;     ///< The component sending the message is the IMU, it could be also a Linux process
  mavlink_system.type = MAV_TYPE_GROUND_ROVER;   ///< This system is an airplane / fixed wing
  
  // Define the system type, in this case an airplane
  uint8_t system_type = MAV_TYPE_GROUND_ROVER;
  uint8_t autopilot_type = MAV_AUTOPILOT_GENERIC;
  
  uint8_t system_mode = MAV_MODE_PREFLIGHT; ///< Booting up
  uint32_t custom_mode = 0;                 ///< Custom mode, can be defined by user/adopter
  uint8_t system_state = MAV_STATE_STANDBY; ///< System ready for flight
  
  // Initialize the required buffers
  mavlink_message_t msg;
  mavlink_message_t msgtest;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  uint8_t buftest[MAVLINK_MAX_PACKET_LEN];  
  uint8_t led[5] = {1,2,3,4,5};
  // Pack the messages
  mavlink_msg_heartbeat_pack(mavlink_system.sysid, mavlink_system.compid, &msg, system_type, autopilot_type, system_mode, custom_mode, system_state);
//   mavlink_msg_order_test_pack(mavlink_system.sysid, mavlink_system.compid, &msgtest, led);
  
  // Copy the message to the send buffer
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavlink_msg_order_test_pack(mavlink_system.sysid, mavlink_system.compid, &msg, led);
  uint16_t lentest = mavlink_msg_to_send_buffer(buftest, &msg);
  
  while (TRUE) {
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
        
    tlcounter +=1;

    if(tlcounter == 5) {
      while(TRUE) {
	if (UARTD2.txstate == UART_TX_IDLE) {
	  uartStartSend(&UARTD2, lentest, buftest);
	  tlcounter = 0;
	  break;
	}
      }
    }
    else {
      while(TRUE) {
	if (UARTD2.txstate == UART_TX_IDLE) {
	  uartStartSend(&UARTD2, len, buf);
	  break;
	}
      }   
    }
//     uartStartSend(&UARTD2, len, buf);

    
    chThdSleepUntil(time);
  }
}
