# List of all the MATLAB Coder files for exported function by Coder Toolbox
MATLABSRC = $(CHIBIOS)/algorithms/MATLAB/observer/observer.c \
            $(CHIBIOS)/algorithms/MATLAB/observer/observer_data.c \
            $(CHIBIOS)/algorithms/MATLAB/observer/observer_initialize.c \
            $(CHIBIOS)/algorithms/MATLAB/observer/observer_terminate.c \
            $(CHIBIOS)/algorithms/MATLAB/observer/rtGetInf.c \
            $(CHIBIOS)/algorithms/MATLAB/observer/rtGetNaN.c \
            $(CHIBIOS)/algorithms/MATLAB/observer/rt_nonfinite.c

# Required include directories
MATLABINC = $(CHIBIOS)/algorithms/MATLAB/observer/
