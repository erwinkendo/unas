/*
 * averaging_filter.c
 *
 * Code generation for function 'averaging_filter'
 *
 * C source code generated on: Fri Jan  4 10:57:45 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "averaging_filter.h"
#include "averaging_filter_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void averaging_filter(const real_T x[1024], real_T y[1024])
{
  int32_T i;
  real_T dv0[15];
  real_T b_y;
  int32_T k;

  /*  y = averaging_filter(x) */
  /*  Take an input vector signal 'x' and produce an output vector signal 'y' with */
  /*  same type and shape as 'x' but filtered. */
  /*  Use a persistent variable 'buffer' that represents a sliding window of */
  /*  16 samples at a time. */
  for (i = 0; i < 1024; i++) {
    /*  Scroll the buffer */
    memcpy(&dv0[0], &buffer[0], 15U * sizeof(real_T));
    memcpy(&buffer[1], &dv0[0], 15U * sizeof(real_T));

    /*  Add a new sample value to the buffer */
    buffer[0] = x[i];

    /*  Compute the current average value of the window and */
    /*  write result */
    b_y = buffer[0];
    for (k = 0; k < 15; k++) {
      b_y += buffer[k + 1];
    }

    y[i] = b_y / 16.0;
  }
}

/* End of code generation (averaging_filter.c) */
