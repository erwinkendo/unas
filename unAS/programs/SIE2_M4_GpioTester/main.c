/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"

static uint32_t time;
static uint32_t delay = 200;

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfga = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgb = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgc = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgd = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 9, PAL_MODE_ALTERNATE(7));		/* Tx.     */
  palSetPadMode(GPIOA, 10, PAL_MODE_ALTERNATE(7));		/* Rx.     */
  
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));		/* Tx.     */
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));		/* Rx.     */
   
  /*
  * PWM1, U15.
  */
  pwmStart(&PWMD1, &pwmcfgb);
  palSetPadMode(GPIOA, 8, PAL_MODE_ALTERNATE(2));       /* C2M4.   Channel 1*/
  palSetPadMode(GPIOA, 11, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M4.   Channel 4 */
  palClearPad(GPIOA, 11);

  /*
  * PWM3, U14 y U12.
  */
  pwmStart(&PWMD3, &pwmcfgc);
  palSetPadMode(GPIOC, 6, PAL_MODE_ALTERNATE(2));      /* C2M3.   Channel 1 */
  palSetPadMode(GPIOC, 7, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M3.   Channel 2 */
  palClearPad(GPIOC, 7);
  
  palSetPadMode(GPIOC, 8, PAL_MODE_ALTERNATE(2));      /* C2M1.   Channel 3 */
  palSetPadMode(GPIOC, 9, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M1.   Channel 4 */  
  palClearPad(GPIOC, 9);

  /*
  * PWM4, U13.
  */
  pwmStart(&PWMD4, &pwmcfgd);
  palSetPadMode(GPIOB, 9, PAL_MODE_ALTERNATE(2));      /* C2M2.   Channel 4 */   
  palSetPadMode(GPIOB, 8, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M2.   Channel 3 */
  palClearPad(GPIOB, 8);

  pwmEnableChannel(&PWMD1, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD1, 7500)); 

  pwmEnableChannel(&PWMD3, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD3, 7500)); 
  pwmEnableChannel(&PWMD3, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD3, 7500)); 

  pwmEnableChannel(&PWMD4, 3, PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 7500)); 

  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */

	palSetPadMode(GPIOD, 2, PAL_MODE_OUTPUT_PUSHPULL);

  while (TRUE) {
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
    
    palTogglePad(GPIOD, 2);
    chprintf((BaseSequentialStream *)&SD1, "Hola desde SIEBOT2 Serial1. %u \r\n", delay);
    chprintf((BaseSequentialStream *)&SD2, "Hola desde SIEBOT2 Serial2. %u \r\n", delay);
        
    chThdSleepUntil(time);
  }
}
