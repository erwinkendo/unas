#!/bin/sh

# Helper Script to easily include exported code form MATLAB Coder Toolbox into unAS software arquitecture

# Fix variables
file_header='# List of all the MATLAB Coder files for exported function by Coder Toolbox'
include_header='# Required include directories'
matlab_cheader='MATLABINC = '
matlab_csource="MATLABSRC =  "
unas_path='$(CHIBIOS)/algorithms/MATLAB'
unas_path_sed='$(CHIBIOS)\/algorithms\/MATLAB'

# Folder with exported C code, if no folder given, it searches in all the current folder
if [ -n "$1" ]; then
    folder=$1
else
    folder='./'
fi

echo "====================================================================================="
echo "==== Helper Script to include MATLAB Coder Toolbox exported code in unAS program ===="
echo "====================================================================================="
printf "====== Generating MATLAB.mk file from content in %20s folder. =======\n" $folder
echo "====================================================================================="
echo

find $folder -name '*.c' > temp1.txt
sed "s/^/"$unas_path_sed"\//gw temp3.txt" temp1.txt
sed ':a;N;$!ba;s/\n/ \\\n            /gw temp2.txt' temp3.txt 

# Final matlab.mk file output
echo $file_header > matlab.mk
echo -n $matlab_csource >> matlab.mk
echo -n ' ' >> matlab.mk
cat temp2.txt >> matlab.mk
echo -e "\n$include_header" >> matlab.mk
echo -n $matlab_cheader>> matlab.mk
echo -e " $unas_path/$folder/" >> matlab.mk

# Erase all temporaly files.
rm temp1.txt
rm temp2.txt
rm temp3.txt