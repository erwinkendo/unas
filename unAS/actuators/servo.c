/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    servo.c
 * @brief   Standar RC Servomotor interface module through PWM code.
 *
 * @addtogroup servo
 * @{
 */
 
#include "ch.h"
#include "hal.h"

#if HAL_USE_PWM || defined(__DOXYGEN__)


/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

/** @brief Pulse offset for initial angle position.*/
static uint8_t pulse_offset = 40;

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Writes a pulse width directly to the selected channel in PWM interface.
 * @pre     The PWM interface must be initialized and the driver started.
 *
 * @param[in] pwm       pointer to the PWM interface
 * @param[in] pin       PWM interface channel
 * @param[in] width     PWM pulse width
 */
void servoWriteWidth(PWMDriver *pwm, uint8_t pin, uint16_t width) {

  pwmEnableChannel(pwm, pin, width);

}

/**
 * @brief   Writes a desired angle for Servo in selected channel in PWM interface.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] pwm       pointer to the PWM interface
 * @param[in] pin       PWM interface channel
 * @param[in] ang       desired position for servo
 */
void servoWriteAng(PWMDriver *pwm, uint8_t pin, uint8_t ang) {

  pwmEnableChannel(pwm, pin, pulse_offset+ang);

}

/**
 * @brief   Read current pulse width for selected channel in PWM interface.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] pwm       pointer to the PWM interface
 * @param[in] pin       PWM interface channel
 * @return              pulse width in ms*100 .
 */
uint16_t servoReadWidth(PWMDriver *pwm, uint8_t pin) {
  
  return pwm->tim->CCR[pin];

}

/**
 * @brief   Reads current servo position for selected channel in PWM interface.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] pwm       pointer to the PWM interface
 * @param[in] pin       PWM interface channel
 * @return              servo angle in degrees.
 */
uint16_t servoReadAng(PWMDriver *pwm, uint8_t pin) {
  
  return (pwm->tim->CCR[pin]-pulse_offset);

}

#endif /* HAL_USE_PWM */ 

/** @} */

