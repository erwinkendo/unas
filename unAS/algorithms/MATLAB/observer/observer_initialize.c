/*
 * observer_initialize.c
 *
 * Code generation for function 'observer_initialize'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "observer.h"
#include "observer_initialize.h"
#include "observer_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void observer_initialize(void)
{
  rt_InitInfAndNaN(8U);
  x_pre_not_empty = FALSE;
}

/* End of code generation (observer_initialize.c) */
