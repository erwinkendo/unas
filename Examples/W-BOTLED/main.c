/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"



static int8_t flag = 1;

/* Triggered when the button is pressed, change variable value. */
static void extcb1(EXTDriver *extp, expchannel_t channel) {

  (void)extp;
  (void)channel;

  if(flag)
    flag = 0;
  else
    flag = 1;

}



/* Structure to config the behaviour for an external interrupt source. */
static const EXTConfig extcfg = {
  {
    {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOA, extcb1},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL}
  }
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the EXT driver 1.
   */
  extStart(&EXTD1, &extcfg);

  


 palSetPadMode(GPIOD, 12, PAL_MODE_OUTPUT_PUSHPULL);     /* Green.   */
  palSetPadMode(GPIOD, 13, PAL_MODE_OUTPUT_PUSHPULL);     /* Orange.  */
 palSetPadMode(GPIOD, 14, PAL_MODE_OUTPUT_PUSHPULL);     /* Red.     */
 palSetPadMode(GPIOD, 15, PAL_MODE_OUTPUT_PUSHPULL);     /* Blue.    */



  

  
   /*
   * Enable button EXT channel.
   */
  extChannelEnable(&EXTD1, 0);
  
  
  

  while (TRUE) {

   
 
    if (flag) {
      palSetPad(GPIOD, 12);
      palSetPad(GPIOD, 14);  
      palClearPad(GPIOD, 13);
      palClearPad(GPIOD, 15);   
    }
    else {
      palClearPad(GPIOD, 12);
      palClearPad(GPIOD, 14); 
      palSetPad(GPIOD, 13);
      palSetPad(GPIOD, 15);
	}
chThdSleepMilliseconds(1);

    }  
}
