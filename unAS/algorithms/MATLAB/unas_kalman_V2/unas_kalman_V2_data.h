/*
 * unas_kalman_V2_data.h
 *
 * Code generation for function 'unas_kalman_V2_data'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

#ifndef __UNAS_KALMAN_V2_DATA_H__
#define __UNAS_KALMAN_V2_DATA_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "unas_kalman_V2_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */
extern real_T x_pre[6];
extern real_T P_pre[36];
extern real_T phi_pre;
extern real_T theta_pre;
extern real_T psi_pre;

/* Variable Definitions */

/* Function Declarations */
#endif
/* End of code generation (unas_kalman_V2_data.h) */
