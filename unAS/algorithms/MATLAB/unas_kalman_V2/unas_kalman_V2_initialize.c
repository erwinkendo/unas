/*
 * unas_kalman_V2_initialize.c
 *
 * Code generation for function 'unas_kalman_V2_initialize'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "unas_kalman_V2.h"
#include "unas_kalman_V2_initialize.h"
#include "unas_kalman_V2_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void unas_kalman_V2_initialize(void)
{
  int32_T i;
  rt_InitInfAndNaN(8U);
  for (i = 0; i < 6; i++) {
    x_pre[i] = 0.0;
  }

  memset(&P_pre[0], 0, 36U * sizeof(real_T));
  phi_pre = 0.0;
  theta_pre = 0.0;
  psi_pre = 0.0;
}

/* End of code generation (unas_kalman_V2_initialize.c) */
