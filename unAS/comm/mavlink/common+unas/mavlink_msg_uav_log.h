// MESSAGE UAV_LOG PACKING

#define MAVLINK_MSG_ID_UAV_LOG 152

typedef struct __mavlink_uav_log_t
{
 uint32_t test_uint32_t; ///<  Test 1. 
 int32_t test_int32_t; ///<  Test 2. 
 float test_float; ///<  Test 4. 
 uint16_t test_uint16_t; ///<  Test 3. 
} mavlink_uav_log_t;

#define MAVLINK_MSG_ID_UAV_LOG_LEN 14
#define MAVLINK_MSG_ID_152_LEN 14



#define MAVLINK_MESSAGE_INFO_UAV_LOG { \
	"UAV_LOG", \
	4, \
	{  { "test_uint32_t", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_uav_log_t, test_uint32_t) }, \
         { "test_int32_t", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_uav_log_t, test_int32_t) }, \
         { "test_float", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_uav_log_t, test_float) }, \
         { "test_uint16_t", NULL, MAVLINK_TYPE_UINT16_T, 0, 12, offsetof(mavlink_uav_log_t, test_uint16_t) }, \
         } \
}


/**
 * @brief Pack a uav_log message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param test_uint32_t  Test 1. 
 * @param test_int32_t  Test 2. 
 * @param test_uint16_t  Test 3. 
 * @param test_float  Test 4. 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_uav_log_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint32_t test_uint32_t, int32_t test_int32_t, uint16_t test_uint16_t, float test_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[14];
	_mav_put_uint32_t(buf, 0, test_uint32_t);
	_mav_put_int32_t(buf, 4, test_int32_t);
	_mav_put_float(buf, 8, test_float);
	_mav_put_uint16_t(buf, 12, test_uint16_t);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, 14);
#else
	mavlink_uav_log_t packet;
	packet.test_uint32_t = test_uint32_t;
	packet.test_int32_t = test_int32_t;
	packet.test_float = test_float;
	packet.test_uint16_t = test_uint16_t;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, 14);
#endif

	msg->msgid = MAVLINK_MSG_ID_UAV_LOG;
	return mavlink_finalize_message(msg, system_id, component_id, 14, 140);
}

/**
 * @brief Pack a uav_log message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message was sent over
 * @param msg The MAVLink message to compress the data into
 * @param test_uint32_t  Test 1. 
 * @param test_int32_t  Test 2. 
 * @param test_uint16_t  Test 3. 
 * @param test_float  Test 4. 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_uav_log_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint32_t test_uint32_t,int32_t test_int32_t,uint16_t test_uint16_t,float test_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[14];
	_mav_put_uint32_t(buf, 0, test_uint32_t);
	_mav_put_int32_t(buf, 4, test_int32_t);
	_mav_put_float(buf, 8, test_float);
	_mav_put_uint16_t(buf, 12, test_uint16_t);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, 14);
#else
	mavlink_uav_log_t packet;
	packet.test_uint32_t = test_uint32_t;
	packet.test_int32_t = test_int32_t;
	packet.test_float = test_float;
	packet.test_uint16_t = test_uint16_t;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, 14);
#endif

	msg->msgid = MAVLINK_MSG_ID_UAV_LOG;
	return mavlink_finalize_message_chan(msg, system_id, component_id, chan, 14, 140);
}

/**
 * @brief Encode a uav_log struct into a message
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param uav_log C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_uav_log_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_uav_log_t* uav_log)
{
	return mavlink_msg_uav_log_pack(system_id, component_id, msg, uav_log->test_uint32_t, uav_log->test_int32_t, uav_log->test_uint16_t, uav_log->test_float);
}

/**
 * @brief Send a uav_log message
 * @param chan MAVLink channel to send the message
 *
 * @param test_uint32_t  Test 1. 
 * @param test_int32_t  Test 2. 
 * @param test_uint16_t  Test 3. 
 * @param test_float  Test 4. 
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_uav_log_send(mavlink_channel_t chan, uint32_t test_uint32_t, int32_t test_int32_t, uint16_t test_uint16_t, float test_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[14];
	_mav_put_uint32_t(buf, 0, test_uint32_t);
	_mav_put_int32_t(buf, 4, test_int32_t);
	_mav_put_float(buf, 8, test_float);
	_mav_put_uint16_t(buf, 12, test_uint16_t);

	_mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_UAV_LOG, buf, 14, 140);
#else
	mavlink_uav_log_t packet;
	packet.test_uint32_t = test_uint32_t;
	packet.test_int32_t = test_int32_t;
	packet.test_float = test_float;
	packet.test_uint16_t = test_uint16_t;

	_mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_UAV_LOG, (const char *)&packet, 14, 140);
#endif
}

#endif

// MESSAGE UAV_LOG UNPACKING


/**
 * @brief Get field test_uint32_t from uav_log message
 *
 * @return  Test 1. 
 */
static inline uint32_t mavlink_msg_uav_log_get_test_uint32_t(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field test_int32_t from uav_log message
 *
 * @return  Test 2. 
 */
static inline int32_t mavlink_msg_uav_log_get_test_int32_t(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  4);
}

/**
 * @brief Get field test_uint16_t from uav_log message
 *
 * @return  Test 3. 
 */
static inline uint16_t mavlink_msg_uav_log_get_test_uint16_t(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  12);
}

/**
 * @brief Get field test_float from uav_log message
 *
 * @return  Test 4. 
 */
static inline float mavlink_msg_uav_log_get_test_float(const mavlink_message_t* msg)
{
	return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Decode a uav_log message into a struct
 *
 * @param msg The message to decode
 * @param uav_log C-struct to decode the message contents into
 */
static inline void mavlink_msg_uav_log_decode(const mavlink_message_t* msg, mavlink_uav_log_t* uav_log)
{
#if MAVLINK_NEED_BYTE_SWAP
	uav_log->test_uint32_t = mavlink_msg_uav_log_get_test_uint32_t(msg);
	uav_log->test_int32_t = mavlink_msg_uav_log_get_test_int32_t(msg);
	uav_log->test_float = mavlink_msg_uav_log_get_test_float(msg);
	uav_log->test_uint16_t = mavlink_msg_uav_log_get_test_uint16_t(msg);
#else
	memcpy(uav_log, _MAV_PAYLOAD(msg), 14);
#endif
}
