/*
 * averaging_filter_data.c
 *
 * Code generation for function 'averaging_filter_data'
 *
 * C source code generated on: Fri Jan  4 10:57:45 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "averaging_filter.h"
#include "averaging_filter_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
real_T buffer[16];

/* Function Declarations */

/* Function Definitions */
/* End of code generation (averaging_filter_data.c) */
