/*
 * unas_kalman_V2.h
 *
 * Code generation for function 'unas_kalman_V2'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

#ifndef __UNAS_KALMAN_V2_H__
#define __UNAS_KALMAN_V2_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "unas_kalman_V2_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void unas_kalman_V2(const real_T z[6], real_T out[3]);
#endif
/* End of code generation (unas_kalman_V2.h) */
