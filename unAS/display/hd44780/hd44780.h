/**
 * @file    chr6d.h
 * @brief   CHR-6d Digital Inertia Measurement Unit interface module through UART header.
 *
 * @addtogroup chr6d
 * @{
 */

#ifndef _CHR6D_H_
#define _CHR6D_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

static const UARTConfig chr6dUARTpcfg = {
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  /* HW dependent part.*/
  115200,
  0,
  0,
  0
};

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
  void chr6dIMUParcing(SerialDriver *  sdp);
  void chr6dRequestData(UARTDriver *uartp);
  int16_t chr6dGetPitch(void);
  int16_t chr6dGetRoll(void);
  int16_t chr6dGetGyroZ(void);
  int16_t chr6dGetGyroY(void);
  int16_t chr6dGetGyroX(void);
  int16_t chr6dGetAccelZ(void);
  int16_t chr6dGetAccelY(void);
  int16_t chr6dGetAccelX(void);
  
#ifdef __cplusplus
}
#endif

#endif /* _CMPS03_H_ */

/** @} */
