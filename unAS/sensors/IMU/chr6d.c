/**
 * @file    chr6d.c
 * @brief   CHR-6d Digital Inertia Measurement Unit interface module through UART code.
 *
 * @addtogroup chr6d
 * @{
 */

#if HAL_USE_UART || defined(__DOXYGEN__)

#include "ch.h"
#include "hal.h"
#include "chr6d.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

static const UARTConfig chr6dUARTpcfg = {
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  /* HW dependent part.*/
  115200,
  0,
  0,
  0
};

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

static uint8_t imucommand[] = {0x73, 0x6E, 0x70, 0x01, 0x00, 0x01, 0x52};
static uint8_t uartinimu[24];

static int16_t pitch = 0;
static int16_t roll = 0;
static int16_t gyroZ = 0;
static int16_t gyroY = 0;
static int16_t gyroX = 0;
static int16_t accelZ = 0;
static int16_t accelY = 0;
static int16_t accelX = 0;

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

void chr6dRequestData(UARTDriver *uartp) {
  
    uartStartReceive(uartp, 22, uartinimu);
    uartStartSend(uartp, 7, imucommand);
    
    /* IMU data parsing. */
    
    if(uartinimu[3] == 0xB7) {
      pitch = uartinimu[7] | (uartinimu[6] << 8);
      roll = uartinimu[9] | (uartinimu[8] << 8);
      gyroZ = uartinimu[11] | (uartinimu[10] << 8);
      gyroY = uartinimu[13] | (uartinimu[12] << 8);
      gyroX = uartinimu[15] | (uartinimu[14] << 8);
      accelZ = uartinimu[17] | (uartinimu[16] << 8);
      accelY = uartinimu[19] | (uartinimu[18] << 8);
      accelX = uartinimu[21] | (uartinimu[20] << 8);
    }
    
}

int16_t chr6dGetPitch(void) {
 
  return pitch;
  
}
int16_t chr6dGetRoll(void) {
 
  return roll;
  
}

int16_t chr6dGetGyroZ(void) {

  return gyroZ;
  
}

int16_t chr6dGetGyroY(void) {

  return gyroY;
  
}

int16_t chr6dGetGyroX(void) {

  return gyroX;
  
}

int16_t chr6dGetAccelZ(void) {
  
  return accelZ;
  
}

int16_t chr6dGetAccelY(void) {
  
  return accelY;

}

int16_t chr6dGetAccelX(void) {
  
  return accelX;

}

#endif

/** @} */
