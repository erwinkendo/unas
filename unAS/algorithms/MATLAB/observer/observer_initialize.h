/*
 * observer_initialize.h
 *
 * Code generation for function 'observer_initialize'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

#ifndef __OBSERVER_INITIALIZE_H__
#define __OBSERVER_INITIALIZE_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "observer_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void observer_initialize(void);
#endif
/* End of code generation (observer_initialize.h) */
