/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _BOARD_H_
#define _BOARD_H_

/*
 * Setup for the Olimex STM32-P107 Rev.A evaluation board.
 */

/*
 * Board identifier.
 */
#define BOARD_SIE2
#define BOARD_NAME              "SIE2 Rev.A"

/*
 * Board frequencies.
 */
#define STM32_LSECLK            0
#define STM32_HSECLK            12000000

/*
 * MCU type, supported types are defined in ./os/hal/platforms/hal_lld.h.
 */
#define STM32F10X_CL

/*
 * IO pins assignments.
 */
#define GPIOD_LED		2

#define HBRIDGE1_GPIOC_P1	9
#define HBRIDGE1_GPIOC_P2	8
#define HBRIDGE2_GPIOB_P1	11
#define HBRIDGE2_GPIOB_P2	8
#define HBRIDGE3_GPIOC_P1	7
#define HBRIDGE3_GPIOC_P2	6
#define HBRIDGE4_GPIOA_P1	8
#define HBRIDGE4_GPIOA_P2	9

#define HBRIDGE1_GPIOC_C1	3
#define HBRIDGE1_GPIOC_C2	2
#define HBRIDGE2_GPIOB_C1	2
#define HBRIDGE2_GPIOB_C2	3
#define HBRIDGE3_GPIOC_C1	7
#define HBRIDGE3_GPIOC_C2	6
#define HBRIDGE4_GPIOA_C1	3
#define HBRIDGE4_GPIOA_C2	0
/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 *
 * The digits have the following meaning:
 *   0 - Analog input.
 *   1 - Push Pull output 10MHz.
 *   2 - Push Pull output 2MHz.
 *   3 - Push Pull output 50MHz.
 *   4 - Digital input.
 *   5 - Open Drain output 10MHz.
 *   6 - Open Drain output 2MHz.
 *   7 - Open Drain output 50MHz.
 *   8 - Digital input with PullUp or PullDown resistor depending on ODR.
 *   9 - Alternate Push Pull output 10MHz.
 *   A - Alternate Push Pull output 2MHz.
 *   B - Alternate Push Pull output 50MHz.
 *   C - Reserved.
 *   D - Alternate Open Drain output 10MHz.
 *   E - Alternate Open Drain output 2MHz.
 *   F - Alternate Open Drain output 50MHz.
 * Please refer to the STM32 Reference Manual for details.
 */

/*
 * Port A setup.
 * Everything input with pull-up except:
 * PA8  - Alternate Output in low level     ( ).
 * PA11 - Alternate Output in low level     ( ).
 */
#define VAL_GPIOACRL            0x48878B44      /*  PA7...PA0 */
#define VAL_GPIOACRH            0x4444444B      /* PA15...PA8 */
#define VAL_GPIOAODR            0xFFFFF6FF

/*
 * Port B setup:
 * PB8  - Alternate Output in low level     ( ).
 * PB9  - Alternate Output in low level     ( ).
 */
#define VAL_GPIOBCRL            0x88844488      /*  PB7...PB0 */
#define VAL_GPIOBCRH            0x38BBB8FF      /* PB15...PB8 */
#define VAL_GPIOBODR            0xFFFFFCFF

/*
 * Port C setup:
 * PC6  - Alternate Output in low level     ( ).
 * PC7  - Alternate Output in low level     ( ).
 * PC8  - Alternate Output in low level     ( ).
 * PC9  - Alternate Output in low level     ( ).
 */
#define VAL_GPIOCCRL            0x334488B8      /*  PC7...PC0 */
#define VAL_GPIOCCRH            0x444B8B88      /* PC15...PC8 */
#define VAL_GPIOCODR            0xFFFFFC3F

/*
 * Port D setup:
 * PD0  - Input with PU     (unconnected).
 * PD1  - Input with PU     (unconnected).
 * PD2  - Input with PU     (unconnected).
 * PD3  - Input with PU     (unconnected).
 * PD4  - Input with PU     (unconnected).
 * PD5  - Alternate output  (USART2 TX, UEXT).
 * PD6  - Input with PU     (USART2 RX, UEXT).
 * PD7  - Push Pull output  (USB_VBUSON).
 * PD8  - Alternate output  (USART2 TX, remapped).
 * PD9  - Normal input      (USART2 RX, remapped).
 * PD10 - Input with PU     (unconnected).
 * PD11 - Normal input      (USART2 CTS, remapped).
 * PD12 - Alternate output  (USART2 RTS, remapped).
 * PD13 - Input with PU     (unconnected).
 * PD14 - Input with PU     (unconnected).
 * PD15 - Input with PU     (unconnected).
 */
#define VAL_GPIODCRL            0x38B88888      /*  PD7...PD0 */
#define VAL_GPIODCRH            0x888B484B      /* PD15...PD8 */
#define VAL_GPIODODR            0xFFFFFFFF

/*
 * Port E setup.
 * Everything input with pull-up except:
 * PE14 - Normal input      (ETH_RMII_MDINT).
 * PE15 - Normal input      (USB_FAULT).
 */
#define VAL_GPIOECRL            0x88888888      /*  PE7...PE0 */
#define VAL_GPIOECRH            0x44888888      /* PE15...PE8 */
#define VAL_GPIOEODR            0xFFFFFFFF

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
