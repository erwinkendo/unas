/*
 * kalmanfilter.h
 *
 * Code generation for function 'kalmanfilter'
 *
 * C source code generated on: Mon Mar  4 18:01:50 2013
 *
 */

#ifndef __KALMANFILTER_H__
#define __KALMANFILTER_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "kalmanfilter_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void kalmanfilter(const real_T z[2], real_T y[2]);
#endif
/* End of code generation (kalmanfilter.h) */
