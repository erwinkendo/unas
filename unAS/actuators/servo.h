/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    servo.h
 * @brief   Standar RC Servomotor interface module through PWM header.
 *
 * @addtogroup servo
 * @{
 */

#ifndef _SERVO_H_
#define _SERVO_H_

#if HAL_USE_PWM || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/**
 * @name    SERVO PWM driver configuration values
 * @{
 */
#define SERVO_FREQUENCY                 100000
#define SERVO_PERIOD                    4000
/** @} */

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
  void servoWriteWidth(PWMDriver *pwm, uint8_t pin, uint16_t width);
  void servoWriteAng(PWMDriver *pwm, uint8_t pin, uint8_t ang);
  uint16_t servoReadWidth(PWMDriver *pwm, uint8_t pin);
  uint16_t servoReadAng(PWMDriver *pwm, uint8_t pin);
#ifdef __cplusplus
}
#endif

#endif /* HAL_USE_PWM */ 

#endif/* _SERVO_H_ */

/** @} */
