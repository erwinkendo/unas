/*
 * rtGetNaN.h
 *
 * Code generation for function 'averaging_filter'
 *
 * C source code generated on: Fri Jan  4 10:57:45 2013
 *
 */

#ifndef __RTGETNAN_H__
#define __RTGETNAN_H__

#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"

extern real_T rtGetNaN(void);
extern real32_T rtGetNaNF(void);

#endif
/* End of code generation (rtGetNaN.h) */
