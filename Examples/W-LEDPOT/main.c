/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"

#define ADC_NUM_CHANNELS   1
#define ADC_BUF_DEPTH      1

static adcsample_t samples[ADC_NUM_CHANNELS * ADC_BUF_DEPTH];
static int8_t flag = 1;
static int16_t pinint[4] = {0,0,0,0};

/* Triggered when the button is pressed, change variable value. */
static void extcb1(EXTDriver *extp, expchannel_t channel) {

  (void)extp;
  (void)channel;

  if(flag)
    flag = 0;
  else
    flag = 1;

}

/* Structure to config the behaviour for an external interrupt source. */
static const EXTConfig extcfg = {
  {
    {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOA, extcb1},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL}
  }
};

/*
 * ADC conversion group.
 * Mode:        Continuous, 1 samples of 1 channels, SW triggered.
 * Channels:    IN1.
 */
static const ADCConversionGroup adcgrpcfg5 = {
  TRUE,
  ADC_NUM_CHANNELS,
  NULL,
  NULL,
  0,                                 /* CR1 */
  ADC_CR2_SWSTART,                   /* CR2 */
  0,                                 /* SMPR1 */
  ADC_SMPR2_SMP_AN1(ADC_SAMPLE_56), /* SMPR2 */
  ADC_SQR1_NUM_CH(ADC_NUM_CHANNELS), /* SQR1 */
  0,                                 /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN1)    /* SQR3 */
};

/*
 * PWM configuration structure.
 * Channels:    All channels enabled.
 */
static PWMConfig pwmcfg = {
  10000,                                  /* 10kHz PWM clock frequency.   */
  100,                                    /* Initial PWM period 1S.       */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  0,
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the EXT driver 1.
   */
  extStart(&EXTD1, &extcfg);

    /*
   * Initializes the ADC driver 1.
   * The pin PA1 on the port GPIOA is programmed as analog input.
   */
  adcStart(&ADCD1, NULL);
  palSetPadMode(GPIOA, 1, PAL_MODE_INPUT_ANALOG);

  
  /*
   * Initializes the PWM driver 4, routes the TIM4 outputs to the board LEDs.
   */
  pwmStart(&PWMD4, &pwmcfg);
  palSetPadMode(GPIOD, 12, PAL_MODE_ALTERNATE(2));      /* Green.   */
  palSetPadMode(GPIOD, 13, PAL_MODE_ALTERNATE(2));      /* Orange.  */
  palSetPadMode(GPIOD, 14, PAL_MODE_ALTERNATE(2));      /* Red.     */
  palSetPadMode(GPIOD, 15, PAL_MODE_ALTERNATE(2));      /* Blue.    */
  
  /*
   * Starts an ADC continuous conversion.
   */
  adcStartConversion(&ADCD1, &adcgrpcfg5, samples, ADC_BUF_DEPTH);
  
   /*
   * Enable button EXT channel.
   */
  extChannelEnable(&EXTD1, 0);
  
  
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));
  
  /*
   * Normal main() thread activity, in this demo it changes the LEDS intensienables and disables the
   * button EXT channel using 5 seconds intervals.
   */
  while (TRUE) {
    
    /* Condition for intensity variation or LED glowing order cases. */
    if (flag) {
      /* Intensity variation case. */
      pinint[0] = PWM_FRACTION_TO_WIDTH(&PWMD4, 5000, samples[0]);
      pinint[1] = pinint[0];
      pinint[2] = pinint[0];
      pinint[3] = pinint[0];
      
    }
    else {
      /* LED glowing order case. */
      if (samples[0] > 3000) {
	pinint[0] = PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 10000);
	pinint[1] = pinint[0];
	pinint[2] = pinint[0];
	pinint[3] = pinint[0];
      }
      else if (samples[0] > 2000) {
	pinint[0] = PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 10000);
	pinint[1] = pinint[0];
	pinint[2] = pinint[0];
	pinint[3] = 0;
      }
      else if (samples[0] > 1000) {
	pinint[0] = PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 10000);
	pinint[1] = pinint[0];
	pinint[2] = 0;
	pinint[3] = 0;   
      }
      else if (samples[0] > 500) {
	pinint[0] = PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 10000);
	pinint[1] = 0;
	pinint[2] = 0;
	pinint[3] = 0;	
      }
      else {
	pinint[0] = 0;
	pinint[1] = 0;
	pinint[2] = 0;
	pinint[3] = 0;
	
      }
      
    }
    /* Change duty cycle for every output pin. */
    pwmEnableChannel(&PWMD4, 0, pinint[0]);
    pwmEnableChannel(&PWMD4, 1, pinint[1]);
    pwmEnableChannel(&PWMD4, 2, pinint[2]);
    pwmEnableChannel(&PWMD4, 3, pinint[3]);
    chprintf((BaseSequentialStream *)&SD2, "OP %u %u %u %d \r\n", samples[0], pinint[0], PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 10000), flag);
//     chThdSleepMilliseconds(100);
  }
}
