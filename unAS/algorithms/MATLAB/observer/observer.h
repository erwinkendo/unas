/*
 * observer.h
 *
 * Code generation for function 'observer'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

#ifndef __OBSERVER_H__
#define __OBSERVER_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "observer_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void observer(const real_T u[2], real_T y[3]);
#endif
/* End of code generation (observer.h) */
