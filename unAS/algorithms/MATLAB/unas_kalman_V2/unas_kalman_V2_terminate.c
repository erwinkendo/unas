/*
 * unas_kalman_V2_terminate.c
 *
 * Code generation for function 'unas_kalman_V2_terminate'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "unas_kalman_V2.h"
#include "unas_kalman_V2_terminate.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void unas_kalman_V2_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (unas_kalman_V2_terminate.c) */
