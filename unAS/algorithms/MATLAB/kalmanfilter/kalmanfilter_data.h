/*
 * kalmanfilter_data.h
 *
 * Code generation for function 'kalmanfilter_data'
 *
 * C source code generated on: Mon Mar  4 18:01:50 2013
 *
 */

#ifndef __KALMANFILTER_DATA_H__
#define __KALMANFILTER_DATA_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "kalmanfilter_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */
extern real_T x_est[6];
extern real_T p_est[36];

/* Variable Definitions */

/* Function Declarations */
#endif
/* End of code generation (kalmanfilter_data.h) */
