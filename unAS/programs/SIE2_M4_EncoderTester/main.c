/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"

static uint32_t time;
static uint32_t delay = 10;

/*
 * ICU configuration structure.
 * Quadratue encoder mode enabled, the active state is a logic one.
 */
static ICUConfig icuQEbcfg = {
  ICU_INPUT_ACTIVE_HIGH,
  20000000,                                    /* 20Mz ICU clock frequency.   */
  NULL,
  NULL,
  NULL,
  ICU_CHANNEL_3                                /* HAL Hack, Quadrature ICU mode.   */
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 9, PAL_MODE_ALTERNATE(7));		/* Tx.     */
  palSetPadMode(GPIOA, 10, PAL_MODE_ALTERNATE(7));		/* Rx.     */

  /*
  * Activates the ICU driver 3 using the configuration structure icuQEbcfg.
  */  
  palSetPadMode(GPIOA, 6, PAL_MODE_ALTERNATE(2));				/* FP6.		QE1. */
  palSetPadMode(GPIOA, 7, PAL_MODE_ALTERNATE(2));				/* FP7.		QE2. */
  icuStart(&ICUD3, &icuQEbcfg);
  icuEnable(&ICUD3);
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */

  palSetPadMode(GPIOD, 2, PAL_MODE_OUTPUT_PUSHPULL);

  chprintf((BaseSequentialStream *)&SD1, "Hola desde SIEBOT2 Serial1.\r\n");
  
  while (TRUE) {
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
    
    palTogglePad(GPIOD, 2);
  
    chprintf((BaseSequentialStream *)&SD1, "%u \r\n", ICUD3.tim->CNT);
    chThdSleepUntil(time);
  }
}
