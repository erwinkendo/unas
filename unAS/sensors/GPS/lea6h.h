/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    lea6h.h
 * @brief   uBlox LEA6H GPS interface module through UART header.
 *
 * @addtogroup lea6h
 * @{
 */

#ifndef _LEA6H_H_
#define _LEA6H_H_

#if HAL_USE_UART || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

/**
 * @brief    uBlox LEA6H GPS UART configuration structure
 */
static const UARTConfig lea6hUARTpcfg;

#ifdef __cplusplus
extern "C" {
#endif
  void lea6hUARTFun(UARTDriver *uartp, uint16_t c);
  uint32_t lea6hGetTime(void);
  int32_t lea6hGetLatitude(void);
  int32_t lea6hGetLongitude(void);
  int32_t lea6hGetHeight(void);
  int32_t lea6hGetHeightMSL(void);
  uint32_t lea6hGetHAcc(void);
  uint32_t lea6hGetVAcc(void);
#ifdef __cplusplus
}
#endif

#endif /* HAL_USE_UART */

#endif /* _LEA6H_H_ */

/** @} */
