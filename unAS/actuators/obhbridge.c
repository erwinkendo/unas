/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    obhbridge.c
 * @brief   SIE2 On Board H-Bridge interface module through PWM code.
 *
 * @addtogroup obhbridge
 * @{
 */
 
#include "ch.h"
#include "hal.h"
#include "obhbridge.h"

#if HAL_USE_PWM || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

/** @brief Pulse offset for initial angle position.*/
static uint8_t output1dir;
static uint8_t output2dir;
static uint8_t output3dir;
static uint8_t output4dir;

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Changes H-Bridge current flow direction ClockWise maintaining a PWM width.
 * @pre     The PWM interface must be initialized and the driver started.
 *
 * @param[in] output    H-Bridge device whose direction to change
 * @param[in] width     PWM pulse width
 */
void obhbridgeTurnCW(uint8_t output, uint16_t width) {

	switch(output) {
    case obhbridge1:
		#if STM32_PWM_USE_TIM3

		palSetPadMode(GPIOC, 8, PAL_MODE_OUTPUT_PUSHPULL);      /* C2M1.   Channel 3 */
		palClearPad(GPIOC, 8);
		palSetPadMode(GPIOC, 9, PAL_MODE_ALTERNATE(2));      /* C1M1.   Channel 4 */  
		pwmEnableChannel(&PWMD3, 3, width);
		output1dir = 1;
		
		#endif
		break;
	case obhbridge2:
		#if STM32_PWM_USE_TIM4
		
		palSetPadMode(GPIOB, 9, PAL_MODE_OUTPUT_PUSHPULL);      /* C2M2.   Channel 4 */ 
		palClearPad(GPIOB, 9);
		palSetPadMode(GPIOB, 8, PAL_MODE_ALTERNATE(2));      /* C1M2.   Channel 3 */
		pwmEnableChannel(&PWMD4, 2, width);
		output2dir = 1;
		
		#endif
		break;
	case obhbridge3: 
		#if STM32_PWM_USE_TIM3
		
		palSetPadMode(GPIOC, 6, PAL_MODE_OUTPUT_PUSHPULL);      /* C2M3.   Channel 1 */
		palClearPad(GPIOC, 6);
		palSetPadMode(GPIOC, 7, PAL_MODE_ALTERNATE(2));      /* C1M3.   Channel 2 */
		pwmEnableChannel(&PWMD3, 1, width);
		output3dir = 1;
		
		#endif
		break;	
	case obhbridge4:
		#if STM32_PWM_USE_TIM1
		
		palSetPadMode(GPIOA, 8, PAL_MODE_OUTPUT_PUSHPULL);       /* C2M4.   Channel 1*/
		palClearPad(GPIOA, 8);
		palSetPadMode(GPIOA, 11, PAL_MODE_ALTERNATE(1));      /* C1M4.   Channel 4 */
		pwmEnableChannel(&PWMD1, 3, width);
		output4dir = 1;
		
		#endif
		break;
	}

}

/**
 * @brief   Changes H-Bridge current flow direction CounterClockWise maintaining a PWM width.
 * @pre     The PWM interface must be initialized and the driver started.
 *
 * @param[in] output    H-Bridge device whose direction to change
 * @param[in] width     PWM pulse width
 */
void obhbridgeTurnCCW(uint8_t output, uint16_t width) {

	switch(output) {
    case obhbridge1:
		#if STM32_PWM_USE_TIM3
	
		palSetPadMode(GPIOC, 9, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M1.   Channel 4 */  
		palClearPad(GPIOC, 9);
		palSetPadMode(GPIOC, 8, PAL_MODE_ALTERNATE(2));      /* C2M1.   Channel 3 */
		pwmEnableChannel(&PWMD3, 2, width);
		output1dir = 2;
		
		#endif
		break;
	case obhbridge2:
		#if STM32_PWM_USE_TIM4
	
		palSetPadMode(GPIOB, 8, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M2.   Channel 3 */
		palClearPad(GPIOB, 8);
		palSetPadMode(GPIOB, 9, PAL_MODE_ALTERNATE(2));      /* C2M2.   Channel 4 */ 
		pwmEnableChannel(&PWMD4, 3, width);
		output2dir = 2;
		
		#endif
		break;
	case obhbridge3: 
		#if STM32_PWM_USE_TIM3
	
		palSetPadMode(GPIOC, 7, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M3.   Channel 2 */
		palClearPad(GPIOC, 7);
		palSetPadMode(GPIOC, 6, PAL_MODE_ALTERNATE(2));      /* C2M3.   Channel 1 */
		pwmEnableChannel(&PWMD3, 0, width);
		output3dir = 2;
		
		#endif
		break;	
	case obhbridge4:
		#if STM32_PWM_USE_TIM1
	
		palSetPadMode(GPIOA, 11, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M4.   Channel 4 */
		palClearPad(GPIOA, 11);
		palSetPadMode(GPIOA, 8, PAL_MODE_ALTERNATE(1));       /* C2M4.   Channel 1*/
		pwmEnableChannel(&PWMD1, 0, width);
		output4dir = 2;
		
		#endif
		break;
	}

}

/**
 * @brief   Stops H-Bridge current flow.
 * @pre     The PWM interface must be initialized and the driver started.
 *
 * @param[in] output    H-Bridge device whose direction to change
 */
void obhbridgeStop(uint8_t output) {
  
	switch(output) {
    case obhbridge1:
		palSetPadMode(GPIOC, 9, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M1.   Channel 4 */  
		palClearPad(GPIOC, 9);
		palSetPadMode(GPIOC, 8, PAL_MODE_OUTPUT_PUSHPULL);      /* C2M1.   Channel 3 */
		palClearPad(GPIOC, 8);
		output1dir = 0;
		break;
	case obhbridge2:
		palSetPadMode(GPIOB, 9, PAL_MODE_OUTPUT_PUSHPULL);      /* C2M2.   Channel 4 */ 
		palClearPad(GPIOB, 9);
		palSetPadMode(GPIOB, 8, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M2.   Channel 3 */
		palClearPad(GPIOB, 8);
		output2dir = 0;
		break;
	case obhbridge3: 
		palSetPadMode(GPIOC, 6, PAL_MODE_OUTPUT_PUSHPULL);      /* C2M3.   Channel 1 */
		palClearPad(GPIOC, 6);
		palSetPadMode(GPIOC, 7, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M3.   Channel 2 */
		palClearPad(GPIOC, 7);
		output3dir = 0;
		break;	
	case obhbridge4:
		palSetPadMode(GPIOA, 8, PAL_MODE_OUTPUT_PUSHPULL);       /* C2M4.   Channel 1*/
		palClearPad(GPIOA, 8);
		palSetPadMode(GPIOA, 11, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M4.   Channel 4 */
		palClearPad(GPIOA, 11);
		output4dir = 0;
		break;
	}

}

/**
 * @brief   Changes H-Bridge PWM width maintaining current flow direction.
 * @pre     The PWM interface must be initialized and the driver started.
 *
 * @param[in] output    H-Bridge device whose PWM to change
 * @param[in] width     PWM pulse width
 */
void obhbridgeSetPWM(uint8_t output, uint16_t width) {
switch(output) {
    case obhbridge1:
		#if STM32_PWM_USE_TIM3
	
		if(output1dir) {
			pwmEnableChannel(&PWMD3, 3, width);
		}
		else if(output1dir == 2) {
			pwmEnableChannel(&PWMD3, 2, width);
		}
		
		#endif
		break;
	case obhbridge2:
		#if STM32_PWM_USE_TIM4
	
		if(output2dir) {
			pwmEnableChannel(&PWMD4, 2, width);
		}
		else if(output2dir == 2) {
			pwmEnableChannel(&PWMD4, 3, width);
		}
		
		#endif
		break;
	case obhbridge3: 
		#if STM32_PWM_USE_TIM3
		
		if(output3dir) {
			pwmEnableChannel(&PWMD3, 1, width);
		}
		else if(output3dir == 2) {
			pwmEnableChannel(&PWMD3, 0, width);
		}
		
		#endif
		break;	
	case obhbridge4:
		#if STM32_PWM_USE_TIM1
	
		if(output4dir) {
			pwmEnableChannel(&PWMD1, 3, width);
		}
		else if(output4dir == 2) {
			pwmEnableChannel(&PWMD1, 0, width);
		}
		
		#endif
		break;
	}
}

#endif /* HAL_USE_PWM */ 

/** @} */

