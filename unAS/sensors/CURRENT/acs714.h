/**
 * @file    acs714.h
 * @brief   ACS714 Current Hall Sensor module through ADC header.
 *
 * @addtogroup acs714
 * @{
 */

#ifndef _ACS714_H_
#define _ACS714_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/**
 * @name    Sensor Sensitivity.
 * @{
 */
#define acs714SensitivityV            0.185 /**< @brief (V/A).  */
#define acs714SensitivitymV           185.0 /**< @brief (mV/A). */
/** @} */

/**
 * @name    12-bit ADC Resolution (Helper Data).
 * @{
 */
#define acs714ADCResolutionV          0.000805664  /**< @brief (V/ADC).  */
#define acs714ADCResolutionmV         0.805664	   /**< @brief (mV/ADC).  */ 
/** @} */


/**
 * @name    Hard-Coded Sensor Offset.
 * @{
 */
#define acs714OffsetVolt              2.500 /**< @brief (V).  */
#define acs714OffsetmVolt             2500.0 /**< @brief (mV).  */
#define acs714OffsetADC               3103 /**< @brief (ADC).  */
/** @} */

/**
 * @name    ADC measurement to mVolt conversion.
 * @{
 */
#define acs714OADCtomV(n)       	  ((n) * acs714ADCResolutionmV)
/** @} */

/**
 * @name    mVolt to current conversion.
 * @{
 */
#define acs714OmVtoCurrent(n)         ((n) * (1/acs714SensitivitymV))
/** @} */

/**
 * @name    mVolt to current conversion (Hard-Coded Offset removed).
 * @{
 */
#define acs714OmVtoCurrentOffset(n)   acs714OmVtoCurrent((n) - acs714OffsetmVolt )
/** @} */

/**
 * @name    ADC measurement to current conversion (Hard-Coded Offset removed).
 * @{
 */
#define acs714OADCtoCurrentOffset(n)  acs714OmVtoCurrentOffset(acs714OADCtomV(n))
/** @} */

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


#endif /* _ACS714_H_ */

/** @} */
