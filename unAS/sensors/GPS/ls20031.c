/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    ls20031.c
 * @brief   LS20031 GPS interface module through UART code.
 *
 * @addtogroup ls20031
 * @{
 */

#if HAL_USE_UART || defined(__DOXYGEN__)

#include "ch.h"
#include "hal.h"
#include "ls20031.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

static uint16_t counter = 0;
static uint16_t espcount = 0;
// static uint16_t tibr = 7;
// static uint16_t labr = 30;
// static uint16_t lobr = 18;
// static uint16_t gsbr = 91;
// static uint16_t fqbr = 43;
// static uint16_t nsbr = 45;
// static uint16_t hdbr = 47;
// static uint16_t albr = 52;

static uint8_t time[10];
static uint8_t latitude[11];
static uint8_t longitude[11];
static uint8_t groundSpeed[4];
static uint8_t fixQuality[1];
static uint8_t numberSatellites[2];
static uint8_t hdop[4];
static uint8_t altitude[6];

static uint8_t uartingps[180];
static uint16_t flag = 0;
static uint8_t gpsflag = 0;
static uint8_t gpsconfig;

static uint8_t gpsparline = 0;
static uint8_t gpsparfield = 0;
static uint8_t gpspartempc = 0;

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

static const UARTConfig ls20031UARTpcfg = {
  NULL,
  NULL,
  NULL,
  ls20031UARTFun,
  NULL,
  /* HW dependent part.*/
  57600,
  0,
  0,
  0
};

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

void ls20031UARTFun(UARTDriver *uartp, uint16_t c) {
  
  (void) uartp;

  /* Initial GPS Decoding condition, no previous operation done. */
  if (gpsflag == 0) {
  
    /* First Debugging condition. If first input char is $, start storing data and set the flag gpsflag ON. */
    if (c == '$') {   
      gpsflag = 1;
      uartingps[flag] = c;
      flag ++;
    }
    
  }
  /* Once gpsflag is set, normal operation is done. */
  else {
    /* Second Debugging condition. If third char is R, keep storing, else restart from the beginnig. */
    if (flag == 3) {
    
      if (c == 'G') {
	uartingps[flag] = c;
	flag ++;
      }
      else {
	gpsflag = 0;
	flag = 0;
      }
      
    }
    else {
      uartingps[flag] = c;
      flag ++;
      if(c == 10) {
	espcount++;     
	if (espcount == 2) {
	  gpsflag = 0;
	  flag = 0; 
	  espcount = 0;
	}
      }
    }
  }
}

// // // static uint8_t state = 1;
// // // static uint8_t gpsmesg = 0;
// // // static uint8_t gpscount = 0;
// // // static uint8_t gpstype = 0;
// // // static uint8_t gpsintcount = 0;
// // // static uint8_t gpstore = 0;
// // // 
// // // /* Initial state, syncing for gps message initial character. */
// // // if(state == 1) {
// // //   if(c == '$') {
// // //     state = 2;
// // //     counter = 1;
// // //   }
// // // }
// // // /* Second state, recognicing type of gps message. */
// // // else if(state == 2) {
// // //   if(counter == 4) {
// // //     if(c == 'G') {
// // //       state = 3;
// // //       gpsmesg = 1;
// // //     }
// // //     else if(c == 'S') {
// // //       state = 3;
// // //       gpsmesg = 2;
// // //     }
// // //     else if(c == 'T') {
// // //       state = 3;
// // //       gpsmesg = 3;
// // //     }
// // //     else {
// // //       state = 1;
// // //       counter = 1;
// // //     }
// // //   }
// // // }
// // // /* Third state... on going. */
// // // else if(state == 3) {
// // //   if(c == ',') {
// // //     gpscount++;
// // //     gpstore = 0;
// // //     gpsintcount = 0;
// // //   }
// // //   else if(c == 10) {
// // //     state = 1;
// // //     gpscount = 0;
// // //     gpstore = 0;
// // //   }
// // //   if(gpstore == 0) {
// // //     if(gpsmesg == 1) {
// // //       if(gpscount == 1) {
// // // 	gpstype = 1;
// // // 	gpstore = 1;
// // //       }
// // //       else if(gpscount == 2) {
// // // 	gpstype = 2;
// // // 	gpstore = 1;
// // //       }
// // //       else if(gpscount == 3) {
// // // 	gpstype = 3;
// // // 	gpstore = 1;
// // //       }
// // //       else if(gpscount == 4) {
// // // 	gpstype = 4;
// // // 	gpstore = 1;
// // //       }
// // //     }
// // //     else if(gpsmesg == 2) {
// // //     }
// // //     else if(gpsmesg == 3) {
// // //     }    
// // //   }
// // //   else {
// // //     switch (gpsmesg) {
// // //       case 1:
// // // 	switch (gpstype) {
// // // 	  case 1:
// // // 	    time[gpsintcount] = c;
// // // 	    break;
// // // 	  case 2:
// // // 	    latitude[gpsintcount] = c;
// // // 	    break;    
// // // 	  case 3:
// // // 	    longitude[gpsintcount] = c;
// // // 	    break;      
// // // 	  default:
// // // 	    break;
// // // 	}
// // // 	gpsintcount++;
// // // 	break;
// // //       case 2:
// // // 	break;    
// // //       case 3:
// // // 	break;      
// // //       default:
// // // 	break;
// // //     } 
// // //   }
// // //   
// // // }

//   (void) uartp;
// 
//   /* Initial GPS Decoding condition, no previous operation done. */
//   if (gpsflag == 0) {
//     
//     /* First Debugging condition. If first input char is $, start storing data and set the flag gpsflag ON. */
//     if (c == '$') {
//       gpsflag = 1;
//       uartingps[flag] = c;
//       flag ++;
//     }
//     
//   }
//   /* Once gpsflag is set, normal operation is done. */
//   else if (gpsflag == 1) {
//     /* Second Debugging condition. If third char is R, keep storing, else restart from the beginnig. */
//     if (flag == 3) {
//     
//       if (c == 'G') {
// 	uartingps[flag] = c;
// 	flag ++;
// 	gpsflag = 2;
//       }
//       else if (c == 'S') {
// 	uartingps[flag] = c;
// 	flag ++;
// 	gpsflag = 3;
//       }
//       else if (c == 'T') {
// 	uartingps[flag] = c;
// 	flag ++;
// 	gpsflag = 4;
//       }
//       else {
// 	gpsflag = 0;
// 	flag = 0;	
//       }
//       
//     }
//     else {
//       uartingps[flag] = c;
//       flag ++;
//     }
//   }
//   /* $GPGGA NMEA string. */
//   else if (gpsflag == 2) {
//         
//     /* Count the field separators. */
//     if (c == ',') {
// 	numsep ++;
// 	counter_int = 0;
//     }
//     /* New Line verification. */
//     else if (c == 10) {
// // 	chprintf((BaseSequentialStream *)&SD6, "hola %d %d %d %d %d\n\r", gpsflag, flag, counter_int, numsep, store_enable);
// 	gpsflag = 0;
// 	flag = 0;
// 	counter_int = 0;
// 	numsep = 0;
// // 	store_enable = 0;
// // 	chprintf((BaseSequentialStream *)&SD6, "hola %d %d %d %d %d\n\r", gpsflag, flag, counter_int, numsep, store_enable);
//     }
//     if (store_enable == 1) {
//       if (counter_int != 0) {
// 	switch (numsep) {
// 	  case 1:
// 	    time[counter_int-1] = c;
// 	    counter_int++;
// 	    break;
// 	  case 2:
// 	    latitude[counter_int-1] = c;
// 	    counter_int++;
// 	    break;
// 	  case 3:
// 	    latitude[11] = c;
// 	    counter_int++;
// 	    break;    
// 	  case 4:
// 	    longitude[counter_int-1] = c;
// 	    break;   
// 	  case 5:
// 	    longitude[10] = c;
// 	    break;  
// 	  case 6:
// 	    fixQuality[counter_int-1] = c;
// 	    break;
// 	  case 7:
// 	    numberSatellites[counter_int-1] = c;
// 	    counter_int++;
// 	    break;	
// 	  case 8:
// 	    HDOP[counter_int-1] = c;
// 	    counter_int++;
// 	    break;
// 	  case 9:
// 	    altitude[counter_int-1] = c;
// 	    counter_int++;
// 	    break;
// 	  default:
// 	    break;
// 	}
//       }
//       else counter_int++; 
//     }
//     
//     if (numsep > 1) {
//       store_enable = 1;
//     }
// 
//     uartingps[flag] = c;
//     flag ++;
//     
//   }
//   /* $GPGSA NMEA string. */
// //   else if (gpsflag == 3) {
// //     
// //   }
// //   /* $GPVTG NMEA string. */
// //   else if (gpsflag == 4) {
// //     
// //   }
// }



/**
 * @brief   Reads a register value.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] spip      pointer to the SPI initerface
 * @param[in] reg       register number
 * @return              The register value.
 */
void ls20031GpsParcing(SerialDriver *  sdp) {

  if(gpsconfig == LS20031_BASIC) {
  }
  else if(gpsconfig == LS20031_SPEED) {
    /* GPS data parsing. */
    for (counter = 0; counter < 180; counter++) {
    
      if(uartingps[counter] == ',') {
	gpsparfield++;
	gpspartempc = 0;
      }
      else if(uartingps[counter] == 10) {
	gpsparline++;
	if (gpsparline == 2) {
	  gpsparfield = 0;
	  gpsparline = 0;
	  break;
	}
      }
      else {
	switch (gpsparfield) {
	  case 1:
	    time[gpspartempc] = uartingps[counter];
	    gpspartempc++;
	    break;
	  case 2:
	    latitude[gpspartempc] = uartingps[counter];
	    gpspartempc++;
	    break;
	  case 3:
	    latitude[9] = uartingps[counter];
	    break;    
	  case 4:
	    longitude[gpspartempc] = uartingps[counter];
	    gpspartempc++;
	    break;   
	  case 5:
	    longitude[10] = uartingps[counter];
	    break;  
	  case 6:
	    fixQuality[gpspartempc] = uartingps[counter];
	    break;
	  case 7:
	    if(gpspartempc) {
	      numberSatellites[gpspartempc] = uartingps[counter];
	    }
	    else {
	      numberSatellites[1] = 0;
	      numberSatellites[gpspartempc] = uartingps[counter];
	    }
	    gpspartempc++;
	    break;	
	  case 8:
	    hdop[gpspartempc] = uartingps[counter];
	    gpspartempc++;
	    break;
	  case 9:
	    altitude[gpspartempc] = uartingps[counter];
	    gpspartempc++;
	    break;
	  case 21:
	    groundSpeed[gpspartempc] = uartingps[counter];
	    gpspartempc++;
	    break;
	  default:
	    break;
	}
      }
    }
    
//     for (counter = 0; counter < 4; counter ++) {
//       hdop[counter] = uartingps[hdbr+counter];
//     }
//     for (counter = 0; counter < 10; counter ++) {
//       time[counter] = uartingps[tibr+counter];
//     }    
//     for (counter = 0; counter < 12; counter ++) {
//       latitude[counter] = uartingps[labr+counter];
//     }
//     for (counter = 0; counter < 11; counter ++) {
//       longitude[counter] = uartingps[lobr+counter];
//     }
//     for (counter = 0; counter < 4; counter ++) {
//       groundSpeed[counter] = uartingps[gsbr+counter];
//     }
//     for (counter = 0; counter < 1; counter ++) {
//       fixQuality[counter] = uartingps[fqbr+counter];
//     }
//     for (counter = 0; counter < 1; counter ++) {
//       numberSatellites[counter] = uartingps[nsbr+counter];
//     }
//     for (counter = 0; counter < 6; counter ++) {
//       altitude[counter] = uartingps[albr+counter];
//     } 

  }
  else if(gpsconfig == LS20031_QUALITY) {
  }
  else if(gpsconfig == LS20031_ADVANCED) {
  }
  
    
}

void ls20031OutputConfig(uint8_t cap, UARTDriver *uartp) {
   
  switch (cap) {
    case LS20031_BASIC:
      uartStartSend(uartp, 52, "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n");
      break;
    case LS20031_SPEED:
      uartStartSend(uartp, 52, "$PMTK314,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n");
      break;    
    case LS20031_QUALITY:
      uartStartSend(&uartp, 52, "$PMTK314,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n");
      break;    
    case LS20031_ADVANCED:
      uartStartSend(uartp, 52, "$PMTK314,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
      break;    
    default:
      /* Default configuration. */
      uartStartSend(uartp, 52, "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n");
      break;
  }
  gpsconfig = cap;
}

uint8_t * ls20031GetTimeString(void) {
 
  return time;
  
}
uint8_t * ls20031GetLatitudeString(void) {
 
  return latitude;
  
}

uint8_t * ls20031GetLongitudeString(void) {

  return longitude;
  
}

uint8_t * ls20031GetGroundSpeedString(void) {

  return groundSpeed;
  
}

uint8_t * ls20031GetFixQualityString(void) {

  return fixQuality;
  
}

uint8_t * ls20031GetNumberSatellitesString(void) {
  
  return numberSatellites;
  
}

uint8_t * ls20031GetHdopString(void) {
  
  return hdop;

}

uint8_t * ls20031GetAltitudeString(void) {
  
  return altitude;

}

uint8_t * ls20031GetFULL(void) {
  
  return uartingps;

}
#endif
/** @} */
