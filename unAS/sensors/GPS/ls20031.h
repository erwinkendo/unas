/**
 * @file    ls20031.h
 * @brief   LS20031 GPS interface module through UART header.
 *
 * @addtogroup ls20031
 * @{
 */

#ifndef _LS20031_H_
#define _LS20031_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

#define LS20031_BASIC                 0
#define LS20031_SPEED                 1
#define LS20031_QUALITY               2
#define LS20031_ADVANCED              3

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

static const UARTConfig ls20031UARTpcfg;

#ifdef __cplusplus
extern "C" {
#endif
  void ls20031UARTFun(UARTDriver *uartp, uint16_t c);
  void ls20031GpsParcing(SerialDriver *  sdp);
  void ls20031OutputConfig(uint8_t cap, UARTDriver *uartp);
  uint8_t * ls20031GetTimeString(void);
  uint8_t * ls20031GetLatitudeString(void);
  uint8_t * ls20031GetLongitudeString(void);
  uint8_t * ls20031GetGroundSpeedString(void);
  uint8_t * ls20031GetFixQualityString(void);
  uint8_t * ls20031GetNumberSatellitesString(void);
  uint8_t * ls20031GetHdopString(void);
  uint8_t * ls20031GetAltitudeString(void);
  uint8_t * ls20031GetFULL(void);
#ifdef __cplusplus
}
#endif

#endif /* _LS20031_H_ */

/** @} */
