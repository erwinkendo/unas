// MESSAGE MOVEMENT_COMMAND PACKING

#define MAVLINK_MSG_ID_MOVEMENT_COMMAND 150

typedef struct __mavlink_movement_command_t
{
 uint8_t direction; ///<  Direction for the robot to take. Forward=1, Backward=2, Left=3, Right=4 and Stop=5. 
 uint8_t velocity; ///<  Velocity for the robot to move with. From 0 to 100 in max velocity percentage. TODO: Define modes when velocity can be programmed. 
} mavlink_movement_command_t;

#define MAVLINK_MSG_ID_MOVEMENT_COMMAND_LEN 2
#define MAVLINK_MSG_ID_150_LEN 2



#define MAVLINK_MESSAGE_INFO_MOVEMENT_COMMAND { \
	"MOVEMENT_COMMAND", \
	2, \
	{  { "direction", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_movement_command_t, direction) }, \
         { "velocity", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_movement_command_t, velocity) }, \
         } \
}


/**
 * @brief Pack a movement_command message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param direction  Direction for the robot to take. Forward=1, Backward=2, Left=3, Right=4 and Stop=5. 
 * @param velocity  Velocity for the robot to move with. From 0 to 100 in max velocity percentage. TODO: Define modes when velocity can be programmed. 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_movement_command_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint8_t direction, uint8_t velocity)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[2];
	_mav_put_uint8_t(buf, 0, direction);
	_mav_put_uint8_t(buf, 1, velocity);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, 2);
#else
	mavlink_movement_command_t packet;
	packet.direction = direction;
	packet.velocity = velocity;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, 2);
#endif

	msg->msgid = MAVLINK_MSG_ID_MOVEMENT_COMMAND;
	return mavlink_finalize_message(msg, system_id, component_id, 2, 157);
}

/**
 * @brief Pack a movement_command message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message was sent over
 * @param msg The MAVLink message to compress the data into
 * @param direction  Direction for the robot to take. Forward=1, Backward=2, Left=3, Right=4 and Stop=5. 
 * @param velocity  Velocity for the robot to move with. From 0 to 100 in max velocity percentage. TODO: Define modes when velocity can be programmed. 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_movement_command_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint8_t direction,uint8_t velocity)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[2];
	_mav_put_uint8_t(buf, 0, direction);
	_mav_put_uint8_t(buf, 1, velocity);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, 2);
#else
	mavlink_movement_command_t packet;
	packet.direction = direction;
	packet.velocity = velocity;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, 2);
#endif

	msg->msgid = MAVLINK_MSG_ID_MOVEMENT_COMMAND;
	return mavlink_finalize_message_chan(msg, system_id, component_id, chan, 2, 157);
}

/**
 * @brief Encode a movement_command struct into a message
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param movement_command C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_movement_command_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_movement_command_t* movement_command)
{
	return mavlink_msg_movement_command_pack(system_id, component_id, msg, movement_command->direction, movement_command->velocity);
}

/**
 * @brief Send a movement_command message
 * @param chan MAVLink channel to send the message
 *
 * @param direction  Direction for the robot to take. Forward=1, Backward=2, Left=3, Right=4 and Stop=5. 
 * @param velocity  Velocity for the robot to move with. From 0 to 100 in max velocity percentage. TODO: Define modes when velocity can be programmed. 
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_movement_command_send(mavlink_channel_t chan, uint8_t direction, uint8_t velocity)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[2];
	_mav_put_uint8_t(buf, 0, direction);
	_mav_put_uint8_t(buf, 1, velocity);

	_mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_MOVEMENT_COMMAND, buf, 2, 157);
#else
	mavlink_movement_command_t packet;
	packet.direction = direction;
	packet.velocity = velocity;

	_mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_MOVEMENT_COMMAND, (const char *)&packet, 2, 157);
#endif
}

#endif

// MESSAGE MOVEMENT_COMMAND UNPACKING


/**
 * @brief Get field direction from movement_command message
 *
 * @return  Direction for the robot to take. Forward=1, Backward=2, Left=3, Right=4 and Stop=5. 
 */
static inline uint8_t mavlink_msg_movement_command_get_direction(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field velocity from movement_command message
 *
 * @return  Velocity for the robot to move with. From 0 to 100 in max velocity percentage. TODO: Define modes when velocity can be programmed. 
 */
static inline uint8_t mavlink_msg_movement_command_get_velocity(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Decode a movement_command message into a struct
 *
 * @param msg The message to decode
 * @param movement_command C-struct to decode the message contents into
 */
static inline void mavlink_msg_movement_command_decode(const mavlink_message_t* msg, mavlink_movement_command_t* movement_command)
{
#if MAVLINK_NEED_BYTE_SWAP
	movement_command->direction = mavlink_msg_movement_command_get_direction(msg);
	movement_command->velocity = mavlink_msg_movement_command_get_velocity(msg);
#else
	memcpy(movement_command, _MAV_PAYLOAD(msg), 2);
#endif
}
