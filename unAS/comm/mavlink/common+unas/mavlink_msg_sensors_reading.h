// MESSAGE SENSORS_READING PACKING

#define MAVLINK_MSG_ID_SENSORS_READING 151

typedef struct __mavlink_sensors_reading_t
{
 int8_t mems_accelerometer_X; ///<  Reading from mems accelerometer in X axis. 
 int8_t mems_accelerometer_Y; ///<  Reading from mems accelerometer in Y axis. 
 int8_t mems_accelerometer_Z; ///<  Reading from mems accelerometer in Z axis. 
 uint8_t digital_compass; ///<  Reading from digital compass. 
} mavlink_sensors_reading_t;

#define MAVLINK_MSG_ID_SENSORS_READING_LEN 4
#define MAVLINK_MSG_ID_151_LEN 4



#define MAVLINK_MESSAGE_INFO_SENSORS_READING { \
	"SENSORS_READING", \
	4, \
	{  { "mems_accelerometer_X", NULL, MAVLINK_TYPE_INT8_T, 0, 0, offsetof(mavlink_sensors_reading_t, mems_accelerometer_X) }, \
         { "mems_accelerometer_Y", NULL, MAVLINK_TYPE_INT8_T, 0, 1, offsetof(mavlink_sensors_reading_t, mems_accelerometer_Y) }, \
         { "mems_accelerometer_Z", NULL, MAVLINK_TYPE_INT8_T, 0, 2, offsetof(mavlink_sensors_reading_t, mems_accelerometer_Z) }, \
         { "digital_compass", NULL, MAVLINK_TYPE_UINT8_T, 0, 3, offsetof(mavlink_sensors_reading_t, digital_compass) }, \
         } \
}


/**
 * @brief Pack a sensors_reading message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param mems_accelerometer_X  Reading from mems accelerometer in X axis. 
 * @param mems_accelerometer_Y  Reading from mems accelerometer in Y axis. 
 * @param mems_accelerometer_Z  Reading from mems accelerometer in Z axis. 
 * @param digital_compass  Reading from digital compass. 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_sensors_reading_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       int8_t mems_accelerometer_X, int8_t mems_accelerometer_Y, int8_t mems_accelerometer_Z, uint8_t digital_compass)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[4];
	_mav_put_int8_t(buf, 0, mems_accelerometer_X);
	_mav_put_int8_t(buf, 1, mems_accelerometer_Y);
	_mav_put_int8_t(buf, 2, mems_accelerometer_Z);
	_mav_put_uint8_t(buf, 3, digital_compass);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, 4);
#else
	mavlink_sensors_reading_t packet;
	packet.mems_accelerometer_X = mems_accelerometer_X;
	packet.mems_accelerometer_Y = mems_accelerometer_Y;
	packet.mems_accelerometer_Z = mems_accelerometer_Z;
	packet.digital_compass = digital_compass;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, 4);
#endif

	msg->msgid = MAVLINK_MSG_ID_SENSORS_READING;
	return mavlink_finalize_message(msg, system_id, component_id, 4, 106);
}

/**
 * @brief Pack a sensors_reading message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message was sent over
 * @param msg The MAVLink message to compress the data into
 * @param mems_accelerometer_X  Reading from mems accelerometer in X axis. 
 * @param mems_accelerometer_Y  Reading from mems accelerometer in Y axis. 
 * @param mems_accelerometer_Z  Reading from mems accelerometer in Z axis. 
 * @param digital_compass  Reading from digital compass. 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_sensors_reading_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           int8_t mems_accelerometer_X,int8_t mems_accelerometer_Y,int8_t mems_accelerometer_Z,uint8_t digital_compass)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[4];
	_mav_put_int8_t(buf, 0, mems_accelerometer_X);
	_mav_put_int8_t(buf, 1, mems_accelerometer_Y);
	_mav_put_int8_t(buf, 2, mems_accelerometer_Z);
	_mav_put_uint8_t(buf, 3, digital_compass);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, 4);
#else
	mavlink_sensors_reading_t packet;
	packet.mems_accelerometer_X = mems_accelerometer_X;
	packet.mems_accelerometer_Y = mems_accelerometer_Y;
	packet.mems_accelerometer_Z = mems_accelerometer_Z;
	packet.digital_compass = digital_compass;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, 4);
#endif

	msg->msgid = MAVLINK_MSG_ID_SENSORS_READING;
	return mavlink_finalize_message_chan(msg, system_id, component_id, chan, 4, 106);
}

/**
 * @brief Encode a sensors_reading struct into a message
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param sensors_reading C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_sensors_reading_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_sensors_reading_t* sensors_reading)
{
	return mavlink_msg_sensors_reading_pack(system_id, component_id, msg, sensors_reading->mems_accelerometer_X, sensors_reading->mems_accelerometer_Y, sensors_reading->mems_accelerometer_Z, sensors_reading->digital_compass);
}

/**
 * @brief Send a sensors_reading message
 * @param chan MAVLink channel to send the message
 *
 * @param mems_accelerometer_X  Reading from mems accelerometer in X axis. 
 * @param mems_accelerometer_Y  Reading from mems accelerometer in Y axis. 
 * @param mems_accelerometer_Z  Reading from mems accelerometer in Z axis. 
 * @param digital_compass  Reading from digital compass. 
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_sensors_reading_send(mavlink_channel_t chan, int8_t mems_accelerometer_X, int8_t mems_accelerometer_Y, int8_t mems_accelerometer_Z, uint8_t digital_compass)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[4];
	_mav_put_int8_t(buf, 0, mems_accelerometer_X);
	_mav_put_int8_t(buf, 1, mems_accelerometer_Y);
	_mav_put_int8_t(buf, 2, mems_accelerometer_Z);
	_mav_put_uint8_t(buf, 3, digital_compass);

	_mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS_READING, buf, 4, 106);
#else
	mavlink_sensors_reading_t packet;
	packet.mems_accelerometer_X = mems_accelerometer_X;
	packet.mems_accelerometer_Y = mems_accelerometer_Y;
	packet.mems_accelerometer_Z = mems_accelerometer_Z;
	packet.digital_compass = digital_compass;

	_mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SENSORS_READING, (const char *)&packet, 4, 106);
#endif
}

#endif

// MESSAGE SENSORS_READING UNPACKING


/**
 * @brief Get field mems_accelerometer_X from sensors_reading message
 *
 * @return  Reading from mems accelerometer in X axis. 
 */
static inline int8_t mavlink_msg_sensors_reading_get_mems_accelerometer_X(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int8_t(msg,  0);
}

/**
 * @brief Get field mems_accelerometer_Y from sensors_reading message
 *
 * @return  Reading from mems accelerometer in Y axis. 
 */
static inline int8_t mavlink_msg_sensors_reading_get_mems_accelerometer_Y(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int8_t(msg,  1);
}

/**
 * @brief Get field mems_accelerometer_Z from sensors_reading message
 *
 * @return  Reading from mems accelerometer in Z axis. 
 */
static inline int8_t mavlink_msg_sensors_reading_get_mems_accelerometer_Z(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int8_t(msg,  2);
}

/**
 * @brief Get field digital_compass from sensors_reading message
 *
 * @return  Reading from digital compass. 
 */
static inline uint8_t mavlink_msg_sensors_reading_get_digital_compass(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  3);
}

/**
 * @brief Decode a sensors_reading message into a struct
 *
 * @param msg The message to decode
 * @param sensors_reading C-struct to decode the message contents into
 */
static inline void mavlink_msg_sensors_reading_decode(const mavlink_message_t* msg, mavlink_sensors_reading_t* sensors_reading)
{
#if MAVLINK_NEED_BYTE_SWAP
	sensors_reading->mems_accelerometer_X = mavlink_msg_sensors_reading_get_mems_accelerometer_X(msg);
	sensors_reading->mems_accelerometer_Y = mavlink_msg_sensors_reading_get_mems_accelerometer_Y(msg);
	sensors_reading->mems_accelerometer_Z = mavlink_msg_sensors_reading_get_mems_accelerometer_Z(msg);
	sensors_reading->digital_compass = mavlink_msg_sensors_reading_get_digital_compass(msg);
#else
	memcpy(sensors_reading, _MAV_PAYLOAD(msg), 4);
#endif
}
