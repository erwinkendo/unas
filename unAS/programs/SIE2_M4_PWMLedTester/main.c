/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"

static uint32_t time;
static uint32_t delay = 200;

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfga = {
  10000,                                   /* 10kHz PWM clock frequency.  */
  200,                                     /* PWM period is 200 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 9, PAL_MODE_ALTERNATE(7));		/* Tx.     */
  palSetPadMode(GPIOA, 10, PAL_MODE_ALTERNATE(7));		/* Rx.     */
  
   /*
   * Activates the PWM driver 2 using the configuration structure pwmcfgc.
   */ 
  palSetPadMode(GPIOB, 10, PAL_MODE_ALTERNATE(1));      /* FP28.   Channel 3. */   
  pwmStart(&PWMD2, &pwmcfga);
  pwmEnableChannel(&PWMD2, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 0));	// Initial 
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */

  palSetPadMode(GPIOD, 2, PAL_MODE_OUTPUT_PUSHPULL);

  chprintf((BaseSequentialStream *)&SD1, "Hola desde SIEBOT2 Serial1.\r\n");
  uint16_t pwm = 0;
  uint16_t inc = 1000;
  
  while (TRUE) {
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
    
    palTogglePad(GPIOD, 2);
	
	pwm += inc;
	pwmEnableChannel(&PWMD2, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, pwm));
    chprintf((BaseSequentialStream *)&SD1, "%u \r\n", pwm);  
		
	if(pwm > 8000 || pwm < 1000) {
		inc *= -1;
	}

    chThdSleepUntil(time);
  }
}
