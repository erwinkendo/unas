/**
 * @file    acs711.h
 * @brief   ACS711 Current Hall Sensor module through ADC header.
 *
 * @addtogroup acs711
 * @{
 */

#ifndef _ACS711_H_
#define _ACS711_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/**
 * @name    Sensor Sensitivity.
 * @{
 */
#define acs711SensitivityV            0.090			/**< @brief (V/A).  */
#define acs711SensitivitymV           90.0			/**< @brief (mV/A). */
/** @} */

/**
 * @name    12-bit ADC Resolution (Helper Data).
 * @{
 */
#define acs711ADCResolutionV          0.000805664  /**< @brief (V/ADC).  */
#define acs711ADCResolutionmV         0.805664	   /**< @brief (mV/ADC).  */ 
/** @} */


/**
 * @name    Hard-Coded Sensor Offset.
 * @{
 */
#define acs711OffsetVolt              1.650		/**< @brief (V).  */
#define acs711OffsetmVolt             1650.0	/**< @brief (mV).  */
#define acs711OffsetADC               2048		/**< @brief (ADC).  */
/** @} */

/**
 * @name    ADC measurement to mVolt conversion.
 * @{
 */
#define acs711OADCtomV(n)       	  ((n) * acs711ADCResolutionmV)
/** @} */

/**
 * @name    mVolt to current conversion.
 * @{
 */
#define acs711OmVtoCurrent(n)         ((n) * (1/acs711SensitivitymV))
/** @} */

/**
 * @name    mVolt to current conversion (Hard-Coded Offset removed).
 * @{
 */
#define acs711OmVtoCurrentOffset(n)   acs711OmVtoCurrent((n) - acs711OffsetmVolt )
/** @} */

/**
 * @name    ADC measurement to current conversion (Hard-Coded Offset removed).
 * @{
 */
#define acs711OADCtoCurrentOffset(n)  acs711OmVtoCurrentOffset(acs711OADCtomV(n))
/** @} */

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


#endif /* _ACS711_H_ */

/** @} */
