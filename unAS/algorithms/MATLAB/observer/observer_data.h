/*
 * observer_data.h
 *
 * Code generation for function 'observer_data'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

#ifndef __OBSERVER_DATA_H__
#define __OBSERVER_DATA_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "observer_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */
extern boolean_T x_pre_not_empty;

/* Variable Definitions */

/* Function Declarations */
#endif
/* End of code generation (observer_data.h) */
