/*
 * inv.h
 *
 * Code generation for function 'inv'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

#ifndef __INV_H__
#define __INV_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "unas_kalman_V2_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void inv(const real_T x[9], real_T y[9]);
#endif
/* End of code generation (inv.h) */
