/*
 * averaging_filter_terminate.h
 *
 * Code generation for function 'averaging_filter_terminate'
 *
 * C source code generated on: Fri Jan  4 10:57:45 2013
 *
 */

#ifndef __AVERAGING_FILTER_TERMINATE_H__
#define __AVERAGING_FILTER_TERMINATE_H__
/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rtwtypes.h"
#include "averaging_filter_types.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern void averaging_filter_terminate(void);
#endif
/* End of code generation (averaging_filter_terminate.h) */
