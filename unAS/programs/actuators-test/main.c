/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "servo.h"

static uint32_t time;
static uint32_t delay = 1000;

#define ADC_GRP2_NUM_CHANNELS   1
#define ADC_GRP2_BUF_DEPTH      1

/* ADC measurements array. */
static adcsample_t samples2[ADC_GRP2_NUM_CHANNELS * ADC_GRP2_BUF_DEPTH];

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfga = {
  SERVO_FREQUENCY,                                   /* 10kHz PWM clock frequency.  */
  SERVO_PERIOD,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgb = {
  SERVO_FREQUENCY,                                   /* 10kHz PWM clock frequency.  */
  SERVO_PERIOD,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgc = {
  SERVO_FREQUENCY,                                   /* 10kHz PWM clock frequency.  */
  SERVO_PERIOD,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgd = {
  SERVO_FREQUENCY,                                   /* 10kHz PWM clock frequency.  */
  SERVO_PERIOD,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * ADC conversion group for Preasure Sensor.
 * Mode:        Linear buffer, 1 samples of 1 channel, SW triggered.
 * Channels:    IN9.
 */
static const ADCConversionGroup adcgrpcfg2 = {
  FALSE,
  ADC_GRP2_NUM_CHANNELS,
  NULL,
  NULL,
  0, 0,                         /* CR1, CR2 */
  0, 
  ADC_SMPR2_SMP_AN9(ADC_SAMPLE_1P5) ,
  ADC_SQR1_NUM_CH(ADC_GRP2_NUM_CHANNELS),
  0,                            /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN9)
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 9, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOA, 10, PAL_MODE_INPUT);				/* Rx.     */ 

  /* Preasure Sensor Measure Pin. */
  palSetPadMode(GPIOB, 1, PAL_MODE_INPUT_ANALOG);
  adcStart(&ADCD1, NULL);
  
   /*
  * PWM2, PWM_OUT 2 a 4.
  */
  pwmStart(&PWMD2, &pwmcfga);
  palSetPadMode(GPIOA, 1, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* Channel 2 */
  palSetPadMode(GPIOB, 10, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* Channel 3 */
  palSetPadMode(GPIOB, 11, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* Channel 4 */
  
  /*
  * PWM1, U15.
  */
  pwmStart(&PWMD1, &pwmcfgb);
  palSetPadMode(GPIOA, 8, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* C2M4.   Channel 1*/
  palSetPadMode(GPIOA, 11, PAL_MODE_OUTPUT_PUSHPULL);		/* C1M4.   Channel 4 */
  palClearPad(GPIOA, 11);

  /*
  * PWM3, U14 y U12.
  */
  pwmStart(&PWMD3, &pwmcfgc);
  palSetPadMode(GPIOC, 6, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* C2M3.   Channel 1 */
  palSetPadMode(GPIOC, 7, PAL_MODE_OUTPUT_PUSHPULL);		/* C1M3.   Channel 2 */
  palClearPad(GPIOC, 7);
  
  palSetPadMode(GPIOC, 8, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* C2M1.   Channel 3 */
  palSetPadMode(GPIOC, 9, PAL_MODE_OUTPUT_PUSHPULL);		/* C1M1.   Channel 4 */  
  palClearPad(GPIOC, 9);

  /*
  * PWM4, U13.
  */
  pwmStart(&PWMD4, &pwmcfgd);
  palSetPadMode(GPIOB, 9, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* C2M2.   Channel 4 */   
  palSetPadMode(GPIOB, 8, PAL_MODE_OUTPUT_PUSHPULL);      	/* C1M2.   Channel 3 */
  palClearPad(GPIOB, 8);


  servoWriteAng(&PWMD2, 1, 90);
  servoWriteAng(&PWMD2, 2, 90);
  servoWriteAng(&PWMD2, 3, 90);
//   pwmEnableChannel(&PWMD2, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
//   pwmEnableChannel(&PWMD2, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
//   pwmEnableChannel(&PWMD2, 3, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  servoWriteAng(&PWMD1, 0, 90);
//   pwmEnableChannel(&PWMD1, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  servoWriteAng(&PWMD3, 0, 90);
  servoWriteAng(&PWMD3, 2, 90);
//   pwmEnableChannel(&PWMD3, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
//   pwmEnableChannel(&PWMD3, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  servoWriteAng(&PWMD4, 3, 90);
//   pwmEnableChannel(&PWMD4, 3, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  palSetPadMode(GPIOD, GPIOD_LED, PAL_MODE_OUTPUT_PUSHPULL);
  uint8_t a = 0;
  servoWriteAng(&PWMD2, 1, 0);
  servoWriteAng(&PWMD2, 2, 90);
  servoWriteAng(&PWMD2, 3, 180);
  while (TRUE) {
    for(a = 0; a < 181; a++) {

      time = chTimeNow();
      time += MS2ST(delay);            // Next deadline
      
      adcConvert(&ADCD1, &adcgrpcfg2, samples2, ADC_GRP2_BUF_DEPTH);

      palTogglePad(GPIOD, GPIOD_LED);


      servoWriteAng(&PWMD1, 0, a);
      servoWriteAng(&PWMD3, 0, a);
      servoWriteAng(&PWMD3, 2, a);
      servoWriteAng(&PWMD4, 3, a);
      
      /* Test Dump Stream 2*/
      chprintf((BaseSequentialStream *)&SD1, "S %u Angle 0=%u Angle 90=%u Angle 180=%u Angle 0=%u Angle 90=%u Angle 180=%u\r\n", 
	       samples2[0], servoReadWidth(&PWMD2, 1), servoReadWidth(&PWMD2, 2), servoReadWidth(&PWMD2, 3), 
	       servoReadAng(&PWMD2, 1), servoReadAng(&PWMD2, 2), servoReadAng(&PWMD2, 3)
	      );

      chThdSleepUntil(time);
    }
  }
}
