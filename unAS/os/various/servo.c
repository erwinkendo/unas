/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    servo.c
 * @brief   Standar RC Servomotor interface module through PWM code.
 *
 * @addtogroup servo
 * @{
 */

#include "ch.h"
#include "hal.h"
#include "servo.h"


/**
 * @brief   Reads a register value.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] spip      pointer to the SPI initerface
 * @param[in] reg       register number
 * @return              The register value.
 */
void servoWriteWidth(PWMDriver *pwm, uint8_t pin, uint16_t width) {

  pwmEnableChannel(pwm, pin, width);

}

/**
 * @brief   Writes a value into a register.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] spip      pointer to the SPI initerface
 * @param[in] reg       register number
 * @param[in] value     the value to be written
 */
void servoWriteAng(PWMDriver *pwm, uint8_t pin, uint8_t ang) {

  pwmEnableChannel(pwm, pin, 40+ang);

}

/**
 * @brief   Writes a value into a register.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] spip      pointer to the SPI initerface
 * @param[in] reg       register number
 * @param[in] value     the value to be written
 */
uint16_t servoReadWidth(PWMDriver *pwm, uint8_t pin) {
  
  return pwm->tim->CCR[pin];

}

/**
 * @brief   Reads current Servo position in angles.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] spip      pointer to the SPI initerface
 * @param[in] reg       register number
 * @param[in] value     the value to be written
 */
uint16_t servoReadAng(PWMDriver *pwm, uint8_t pin) {
  
  return (pwm->tim->CCR[pin]-40);

}
/** @} */
