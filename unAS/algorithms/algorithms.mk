# List of all the available types of sensors to interact with.


include $(CHIBIOS)/algorithms/MATLAB/matlab.mk
include $(CHIBIOS)/algorithms/CMSIS/CMSIS.mk


ALGORITHMSRC = $(MATLABSRC) 

# Required include directories
ALGORITHMINC = $(MATLABINC) 

