/**
 * @file    cmps03.c
 * @brief   CMPS03 Digital Compass interface module through I2C code.
 *
 * @addtogroup chr6d
 * @{
 */

#if HAL_USE_I2C || defined(__DOXYGEN__)

#include "ch.h"
#include "hal.h"
#include "cmps03.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

static const I2CConfig cmps03I2Cpcfg = {
  OPMODE_I2C,
  100000,
  STD_DUTY_CYCLE
};

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/
  
// static uint8_t addr = 0xC2;
static uint8_t regAddr[] = {0x01};
static uint8_t regAddrf[] = {0x02,0x03};
static uint8_t rxdata[] = {0, 0};

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/
  
void cmps03RequestData(I2CDriver *i2cp, uint8_t a, uint8_t addr) {
  if (a) {
    i2cMasterTransmitTimeout(i2cp, addr >> 1, regAddrf, 2, rxdata, 2, 10);
  }
  else {
    i2cMasterTransmitTimeout(i2cp, addr >> 1, regAddr, 1, rxdata, 1, 10);
  }
    
}

uint8_t cmps03GetHeading(void) {
 
  return rxdata[0];
  
}

uint16_t cmps03GetHeadingf(void) {

  return ((rxdata[0] << 8 )| rxdata[1]);
}

#endif

/** @} */
