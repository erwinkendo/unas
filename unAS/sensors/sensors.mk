# List of all the available types of sensors to interact with.


include $(CHIBIOS)/sensors/COMPASS/compass.mk
include $(CHIBIOS)/sensors/GPS/gps.mk
include $(CHIBIOS)/sensors/IMU/imu.mk
include $(CHIBIOS)/sensors/CURRENT/current.mk


SENSORSRC = $(COMPASSSRC) $(GPSSRC) $(IMUSRC)

# Required include directories
SENSORINC = $(COMPASSINC) $(GPSINC) $(IMUINC) $(CURRENTINC)

