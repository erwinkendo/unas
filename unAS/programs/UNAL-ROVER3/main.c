/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "lis302dl.h"
#include "cmps03.h"
#include "mavlink.h"
#include "chprintf.h"


// static uint8_t flaga = 0;
static uint8_t tester = 0;
// static uint16_t uartin = 0;
static int8_t memData[3];
static uint32_t delay = 50;
static uint32_t time;

static mavlink_system_t mavlink_system;
static mavlink_message_t msgIn;
static mavlink_status_t statusIn;
static mavlink_message_t msgOut;
static uint8_t mavlinkBuf[MAVLINK_MAX_PACKET_LEN];
static uint16_t mavlinkBufLength;
    
void robotControl(uint8_t direction, uint8_t velocity ) {
  /* Process the UART input and changes the motors PWM to control it. */
  if(direction == MAV_BTROVER_STOP) {
    palClearPad(GPIOD,8);
    palClearPad(GPIOD,9);
    palClearPad(GPIOD,10);
    palClearPad(GPIOD,11);
    pwmEnableChannel(&PWMD4, 2, 0);
    pwmEnableChannel(&PWMD4, 3, 0);
  }
  else {
    pwmEnableChannel(&PWMD4, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD4,velocity*100));
    pwmEnableChannel(&PWMD4, 3, PWM_PERCENTAGE_TO_WIDTH(&PWMD4,velocity*100));
    
    if(direction == MAV_BTROVER_LEFT) {
      /* Forward Motion */
      palSetPad(GPIOD,8);
      palClearPad(GPIOD,9);
      palSetPad(GPIOD,10);
      palClearPad(GPIOD,11);
    }
    else if(direction == MAV_BTROVER_RIGHT) {
      /* Reverse Motion */
      palClearPad(GPIOD,8);
      palSetPad(GPIOD,9);
      palClearPad(GPIOD,10);
      palSetPad(GPIOD,11);
    }
    else if(direction == MAV_BTROVER_BACKWARD) {
      /* Left Side Motion */
      palSetPad(GPIOD,8);
      palClearPad(GPIOD,9);
      palClearPad(GPIOD,10);
      palSetPad(GPIOD,11);  
    }
    else if(direction == MAV_BTROVER_FORWARD) {
      /* Right Side Motion */
      palClearPad(GPIOD,8);
      palSetPad(GPIOD,9);
      palSetPad(GPIOD,10);
      palClearPad(GPIOD,11);
    }
  }
}

void uartfun(UARTDriver *uartp, uint16_t c) {
  (void) uartp;
/* Initial GPS Decoding condition, no previous operation done. */
  if(mavlink_parse_char(MAVLINK_COMM_0, c, &msgIn, &statusIn)) {
    // Handle message
    switch(msgIn.msgid) {
      case MAVLINK_MSG_ID_HEARTBEAT:
// 	palTogglePad(GPIOD, 14);
	break;
      case MAVLINK_MSG_ID_MOVEMENT_COMMAND:
	palTogglePad(GPIOD, 12);
// 	chprintf((BaseSequentialStream *)&SD4, "Velocity %D Direction %d\n\r",
// 		 mavlink_msg_movement_command_get_velocity(&msgIn), mavlink_msg_movement_command_get_direction(&msgIn));

	robotControl(mavlink_msg_movement_command_get_direction(&msgIn), mavlink_msg_movement_command_get_velocity(&msgIn));
	break;
      default:
	break;
    }
  }
}

static const UARTConfig UARTpcfg = {
  NULL,
  NULL,
  NULL,
  uartfun,
  NULL,
  /* HW dependent part.*/
  115200,
  0,
  0,
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfg = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * SPI1 configuration structure.
 * Speed 5.25MHz, CPHA=1, CPOL=1, 8bits frames, MSb transmitted first.
 * The slave select line is the pin GPIOE_CS_SPI on the port GPIOE.
 */
static const SPIConfig spi1cfg = {
  NULL,
  /* HW dependent part.*/
  GPIOE,
  GPIOE_CS_SPI,
  SPI_CR1_BR_0 | SPI_CR1_BR_1 | SPI_CR1_CPOL | SPI_CR1_CPHA
};

static SerialConfig serialcfg = {
  57600,
  0,
  USART_CR2_STOP1_BITS | USART_CR2_LINEN,
  0
};


// icucnt_t last_width, last_period;
// 
// static void icuwidthcb(ICUDriver *icup) {
// 
//   last_width = icuGetWidth(icup);
// }
// 
// static void icuperiodcb(ICUDriver *icup) {
// 
//   last_period = icuGetPeriod(icup);
// }

// static ICUConfig icucfg = {
//   ICU_INPUT_ACTIVE_HIGH,
//   70000000,                                    /* 100kHz ICU clock frequency.   */
//   NULL,
//   NULL,
//   NULL,
//   ICU_CHANNEL_3
// };
// 
// static ICUConfig icuenc = {
//   ICU_INPUT_ACTIVE_HIGH,
//   70000000,                                    /* 100kHz ICU clock frequency.   */
//   NULL,
//   NULL,
//   NULL,
//   ICU_CHANNEL_3
// };

/*===========================================================================*/
/* Initialization and main thread.                                           */
/*===========================================================================*/

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Shell manager initialization.
   */


//   sduObjectInit(&SDU1);
//   sduStart(&SDU1, &serusbcfg);

  /*
   * Activates the USB driver and then the USB bus pull-up on D+.
   * Note, a delay is inserted in order to not have to disconnect the cable
   * after a reset.
   */
//   usbDisconnectBus(serusbcfg.usbp);
//   chThdSleepMilliseconds(1000);
//   usbStart(serusbcfg.usbp, &usbcfg);
//   usbConnectBus(serusbcfg.usbp);


  /*
   * Activates the uart driver 2 using the specified driver configuration.
   * PA2(TX) are routed to USART2.
   * PA3(RX) are routed to USART2.
   */
  uartStart(&UARTD2,&UARTpcfg);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));
  
  /*
   * Activates the serial driver 4 using the driver default configuration.
   * PA0(TX) are routed to UART4.
   */
  sdStart(&SD4, NULL);
  palSetPadMode(GPIOA, 0, PAL_MODE_ALTERNATE(8));
  
  
  sdStart(&SD6, &serialcfg);
  palSetPadMode(GPIOC, 6, PAL_MODE_ALTERNATE(8));
  palSetPadMode(GPIOC, 7, PAL_MODE_ALTERNATE(8));
  
  /*
   * Initializes the SPI driver 1 in order to access the MEMS. The signals
   * are already initialized in the board file.
   */
  spiStart(&SPID1, &spi1cfg);
  
  i2cStart(&I2CD1, &cmps03I2Cpcfg);
  chThdSleepMilliseconds(500);
  /*
   * Initializes the PWM driver 4, routes the TIM4 outputs to the board LEDs.
   */
  pwmStart(&PWMD4, &pwmcfg);
  palSetPadMode(GPIOD, GPIOD_LED4, PAL_MODE_OUTPUT_PUSHPULL);      /* Green.   */
  palSetPadMode(GPIOD, GPIOD_LED3, PAL_MODE_ALTERNATE(2));      /* Orange.  */
  palSetPadMode(GPIOD, GPIOD_LED5, PAL_MODE_ALTERNATE(2));      /* Red.     */
  palSetPadMode(GPIOD, GPIOD_LED6, PAL_MODE_ALTERNATE(2));      /* Blue.    */

//   /* ICU 3 (speed measure) initialization*/
//   icuStart(&ICUD3, &icucfg);
//   palSetPadMode(GPIOC, 6, PAL_MODE_ALTERNATE(2));
//   palSetPadMode(GPIOC, 7, PAL_MODE_ALTERNATE(2));
//   icuEnable(&ICUD3);
//   chThdSleepMilliseconds(2000);
//   
//   /* ICU 1 (position measure) initialization manually*/
//   icuStart(&ICUD2, &icuenc);
//   palSetPadMode(GPIOA, 1, PAL_MODE_ALTERNATE(1));
//   palSetPadMode(GPIOA, 15, PAL_MODE_ALTERNATE(1));
//   icuEnable(&ICUD2);
//   ICUD2.tim->CR1  = TIM_CR1_CEN;
  chThdSleepMilliseconds(2000);
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state, when the button is
   * pressed the test procedure is launched with output on the serial
   * driver 2.
   */
  
  /* PWM duty cycles enabled for test purpose */
//   pwmEnableChannel(&PWMD4, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 1000)); 
  pwmEnableChannel(&PWMD4, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD4, 5000));
  
  /* Pins to control motors direction in 3-wires H-Bridge*/
  palSetPadMode(GPIOD, 8, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(GPIOD, 9, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPad(GPIOD,8);
  palSetPad(GPIOD,9);
  palSetPadMode(GPIOD, 10, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(GPIOD, 11, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPad(GPIOD,10);
  palSetPad(GPIOD,11);
  
    /* LIS302DL initialization.*/
  lis302dlWriteRegister(&SPID1, LIS302DL_CTRL_REG1, 0x43);
  lis302dlWriteRegister(&SPID1, LIS302DL_CTRL_REG2, 0x00);
  lis302dlWriteRegister(&SPID1, LIS302DL_CTRL_REG3, 0x00);
  
  mavlink_system.sysid = 2;                                ///< ID 1 for this airplane
  mavlink_system.compid = MAV_COMP_ID_SYSTEM_CONTROL;     ///< The component sending the message is the IMU, it could be also a Linux process
  mavlink_system.type = MAV_TYPE_GROUND_ROVER;            ///< This system is an airplane / fixed wing

    
  chprintf((BaseSequentialStream *)&SD4, "Starting Bluetooth Rover 3 :D \n\r");
  
  while (TRUE) {
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
//     if(flaga>1) {
//       
//       robotControl(uartin);
//       chprintf((BaseSequentialStream *)&SD4, "%U - MemX %D - MemY %D - MemZ %D COMPASS %D \n\r",uartin, memData[0], memData[1], memData[2], cmps03GetHeading());
//       flaga = 0 ;
//       uartin = 0;
//     
//     }
    
    cmps03RequestData(&I2CD1,0,cmps03Adrr0);

    /* Reading MEMS accelerometer X and Y registers.*/
    memData[0] = (int8_t)lis302dlReadRegister(&SPID1, LIS302DL_OUTX);
    memData[1] = (int8_t)lis302dlReadRegister(&SPID1, LIS302DL_OUTY);
    memData[2] = (int8_t)lis302dlReadRegister(&SPID1, LIS302DL_OUTZ);
    
    tester+=1;
    /* Debug Channel*/
    mavlink_msg_sensors_reading_pack(mavlink_system.sysid, mavlink_system.compid, &msgOut, memData[0], memData[1], memData[2], cmps03GetHeading());
    mavlinkBufLength = mavlink_msg_to_send_buffer(mavlinkBuf, &msgOut);
    
    while(TRUE) {
	if (UARTD2.txstate == UART_TX_IDLE) {
	  uartStartSend(&UARTD2, mavlinkBufLength, mavlinkBuf);
	  break;
	}
      }      
//     chprintf((BaseSequentialStream *)&SD4, "sigo vivo \n\r");
    
    chprintf((BaseSequentialStream *)&SD4, "MemX %D - MemY %D - MemZ %D COMPASS %d \n\r",memData[0], memData[1], memData[2], cmps03GetHeading());
    chThdSleepUntil(time);
  }
}
