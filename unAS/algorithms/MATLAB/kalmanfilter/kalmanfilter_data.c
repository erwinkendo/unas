/*
 * kalmanfilter_data.c
 *
 * Code generation for function 'kalmanfilter_data'
 *
 * C source code generated on: Mon Mar  4 18:01:50 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "kalmanfilter.h"
#include "kalmanfilter_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
real_T x_est[6];
real_T p_est[36];

/* Function Declarations */

/* Function Definitions */
/* End of code generation (kalmanfilter_data.c) */
