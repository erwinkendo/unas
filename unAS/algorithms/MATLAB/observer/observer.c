/*
 * observer.c
 *
 * Code generation for function 'observer'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "observer.h"
#include "observer_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
static real_T x_pre[3];
static real_T A[9];
static real_T B[6];
static real_T C[9];
static real_T D[6];

/* Function Declarations */

/* Function Definitions */
void observer(const real_T u[2], real_T y[3])
{
  int32_T i;
  static const real_T dv0[9] = { 0.9989, -4.318E-6, 8.664E-6, 0.001919, 0.08991,
    -0.1804, 2.298E-5, 0.001106, -0.002219 };

  int32_T i0;
  static const real_T dv1[6] = { 0.001073, 4.318E-6, -8.664E-6, 0.0004094, 0.121,
    0.02399 };

  real_T dv2[3];
  real_T dv3[3];
  real_T x[3];
  real_T dv4[3];
  real_T dv5[3];
  if (!x_pre_not_empty) {
    for (i = 0; i < 3; i++) {
      x_pre[i] = 0.0;
    }

    x_pre_not_empty = TRUE;

    /*  variables of the previous x */
    memcpy(&A[0], &dv0[0], 9U * sizeof(real_T));
    for (i0 = 0; i0 < 6; i0++) {
      B[i0] = dv1[i0];
    }

    memset(&C[0], 0, 9U * sizeof(real_T));
    for (i = 0; i < 3; i++) {
      C[i + 3 * i] = 1.0;
    }

    for (i0 = 0; i0 < 6; i0++) {
      D[i0] = 0.0;
    }
  }

  for (i = 0; i < 3; i++) {
    x[i] = x_pre[i];
    dv2[i] = 0.0;
    for (i0 = 0; i0 < 3; i0++) {
      dv2[i] += A[i + 3 * i0] * x_pre[i0];
    }

    dv3[i] = 0.0;
    for (i0 = 0; i0 < 2; i0++) {
      dv3[i] += B[i + 3 * i0] * u[i0];
    }
  }

  for (i0 = 0; i0 < 3; i0++) {
    x_pre[i0] = dv2[i0] + dv3[i0];
    dv4[i0] = 0.0;
    for (i = 0; i < 3; i++) {
      dv4[i0] += C[i0 + 3 * i] * x[i];
    }

    dv5[i0] = 0.0;
    for (i = 0; i < 2; i++) {
      dv5[i0] += D[i0 + 3 * i] * u[i];
    }

    y[i0] = dv4[i0] + dv5[i0];
  }
}

/* End of code generation (observer.c) */
