/*
 * kalmanfilter_terminate.c
 *
 * Code generation for function 'kalmanfilter_terminate'
 *
 * C source code generated on: Mon Mar  4 18:01:50 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "kalmanfilter.h"
#include "kalmanfilter_terminate.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void kalmanfilter_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (kalmanfilter_terminate.c) */
