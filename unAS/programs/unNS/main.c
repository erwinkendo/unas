/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "mavlink.h"
#include "unas_kalman_V2.h"
#include "unas_kalman_V2_initialize.h"

#define ADC_GRP1_NUM_CHANNELS   6
#define ADC_GRP1_BUF_DEPTH      1

#define ADC_GRP2_NUM_CHANNELS   1
#define ADC_GRP2_BUF_DEPTH      1

/* ADC measurements array. */
static adcsample_t samples1[ADC_GRP1_NUM_CHANNELS * ADC_GRP1_BUF_DEPTH];
static adcsample_t samples2[ADC_GRP2_NUM_CHANNELS * ADC_GRP2_BUF_DEPTH];

/* SPI IMU values. */
static uint16_t bursttxbuf[14] = {0x4200, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
static uint16_t burstrxbuf[14] = {0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
// static uint16_t imutxbuf[1] = {0x5600};
// static uint16_t imurxbuf[1] = {0x0000};

/* IMU Auxiliary variables. */
static float gx;
static float gy;
static float gz;
static float ax;
static float ay;
static float az;
// static float mx;
// static float my;
// static float mz;

/* GPS auxiliary and retreived values. */

static uint8_t uartingps[180];
static uint16_t flag = 0;
static uint16_t length = 0;
static uint8_t plcounter = 0;

/* GPS data */
static uint8_t class = 0;
static uint8_t id = 0;
static uint8_t ck_a = 0;
static uint8_t ck_b = 0;
static uint32_t iTOW = 0;
static int32_t lon = 0;
static int32_t lat = 0;
static int32_t height = 0;
static int32_t hMSL = 0;
static uint32_t hAcc = 0;
static uint32_t vAcc = 0;
static uint8_t gpsFix = 0;
static int32_t velN = 0;
static int32_t velE = 0;
static int32_t velD = 0;
static uint32_t speed = 0;
static uint32_t gspeed = 0;
static int32_t heading = 0;
static uint32_t sAcc = 0;
static uint32_t cAcc = 0;

static uint32_t time;
static uint32_t delay = 20;

static uint8_t inputsd[16];
uint8_t tlcounter = 0;

/* Kalman Variables. */
static real_T kalman_v2_in[6];
static real_T kalman_v2_out[3];

/* MAVLINK related variables. */
static mavlink_message_t mavlinkMsg;
static uint8_t mavlinkBuffer[MAVLINK_MAX_PACKET_LEN];
static mavlink_system_t mavlinkSystem;
static uint16_t mavlinkLength;
static uint8_t mavlinkSystemType = MAV_TYPE_FIXED_WING;
static uint8_t mavlinkAutopilotType = MAV_AUTOPILOT_GENERIC;
static uint8_t mavlinkSystemMode = MAV_MODE_PREFLIGHT; ///< Booting up
static uint32_t mavlinkCustomMode = 0;                 ///< Custom mode, can be defined by user/adopter
static uint8_t mavlinkSystemState = MAV_STATE_STANDBY; ///< System ready for flight

static uint8_t prueba[103];
// static uint16_t enviado = 0;
static uint8_t prueba_header[4] = {0x53, 0x4f, 0x46, 0x0a};

/*
 * ADC conversion group for Servo Positions.
 * Mode:        Linear buffer, 1 samples of 6 channel, SW triggered.
 * Channels:    IN10, IN11, IN12, IN13, IN14, IN15.
 */
static const ADCConversionGroup adcgrpcfg1 = {
  FALSE,
  ADC_GRP1_NUM_CHANNELS,
  NULL,
  NULL,
  0, 0,                         /* CR1, CR2 */
  ADC_SMPR1_SMP_AN10(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN11(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN12(ADC_SAMPLE_1P5) | 
  ADC_SMPR1_SMP_AN13(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN14(ADC_SAMPLE_1P5) | ADC_SMPR1_SMP_AN15(ADC_SAMPLE_1P5), 
  0,
  ADC_SQR1_NUM_CH(ADC_GRP1_NUM_CHANNELS),
  0,                            /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN10) | ADC_SQR3_SQ2_N(ADC_CHANNEL_IN11) | ADC_SQR3_SQ3_N(ADC_CHANNEL_IN12) | 
  ADC_SQR3_SQ4_N(ADC_CHANNEL_IN13) | ADC_SQR3_SQ5_N(ADC_CHANNEL_IN14) | ADC_SQR3_SQ6_N(ADC_CHANNEL_IN15) 
};

/*
 * ADC conversion group for Preasure Sensor.
 * Mode:        Linear buffer, 1 samples of 1 channel, SW triggered.
 * Channels:    IN9.
 */
static const ADCConversionGroup adcgrpcfg2 = {
  FALSE,
  ADC_GRP2_NUM_CHANNELS,
  NULL,
  NULL,
  0, 0,                         /* CR1, CR2 */
  0, 
  ADC_SMPR2_SMP_AN9(ADC_SAMPLE_1P5) ,
  ADC_SQR1_NUM_CH(ADC_GRP2_NUM_CHANNELS),
  0,                            /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN9)
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfga = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgb = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgc = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfgd = {
  100000,                                   /* 10kHz PWM clock frequency.  */
  800,                                      /* PWM period is 128 cycles.    */
  NULL,
  {
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL},
   {PWM_OUTPUT_ACTIVE_HIGH, NULL}
  },
  /* HW dependent part.*/
  0
};

/* 
 * UBX TIM-TP,  Size  24,  'Time Pulse'
 * UBX NAV-SOL,  Size  60,  'Navigation Solution'
 * UBX NAV-STATUS,  Size  24,  'Navigation Status'
 * UBX NAV-POSLLH,  Size  36,  'Geodetic Position'
 * UBX NAV-VELNED,  Size  44,  'Velocity in WGS 84'
 */

void uartfun(UARTDriver *uartp, uint16_t c) {
  (void) uartp;

  /* Initial GPS Decoding condition, no previous operation done. */
  switch(flag) {
    case 0:
      if (c == 0xB5) {   
	flag ++;
      }
      break;
    case 1:
      if (c == 0X62) { 
	flag ++;	
      }
      break;
    case 2:
      class = c;
      ck_b = ck_a = c;
      flag ++;
      break;
    case 3:
      id = c;
      ck_b += (ck_a += c); 
      flag ++;
      break;
    case 4:
      length = c;
      ck_b += (ck_a += c); 
      flag ++;
      break;
    case 5:
      length += (uint16_t)(c << 8);
      ck_b += (ck_a += c);
      flag ++;
      if (length > 512) {
	flag = 0;
      }
      plcounter = 0;
      break;
    case 6:
      ck_b += (ck_a += c);
      uartingps[plcounter] = c;
      if (++plcounter == length) {
	flag ++;	
      }
      break;
    case 7:
      flag ++;
      if (ck_a != c) {
	flag = 0;
      }
      break;
    case 8:
      flag = 0;
      if (ck_b == c) {
	/* UBX NAV-POSLLH */
	if (class == 0x01 && id == 0x02) {
	  
	  iTOW = (uint32_t)(uartingps[0] + (uartingps[1] << 8) + (uartingps[2] << 16) + (uartingps[3] << 24));
	  lon = (int32_t)(uartingps[4] | (uartingps[5] << 8) | (uartingps[6] << 16) | (uartingps[7] << 24));
	  lat = (int32_t)(uartingps[8] | (uartingps[9] << 8) | (uartingps[10] << 16) | (uartingps[11] << 24));
	  height = (int32_t)(uartingps[12] + (uartingps[13] << 8) + (uartingps[14] << 16) + (uartingps[15] << 24));
	  hMSL = (int32_t)(uartingps[16] + (uartingps[17] << 8) + (uartingps[18] << 16) + (uartingps[19] << 24));
	  hAcc = (uint32_t)(uartingps[20] + (uartingps[21] << 8) + (uartingps[22] << 16) + (uartingps[23] << 24));
	  vAcc = (uint32_t)(uartingps[24] + (uartingps[25] << 8) + (uartingps[26] << 16) + (uartingps[27] << 24));
	}
	/* UBX NAV-STATUS */
	if (class == 0x01 && id == 0x03) {
	  
	  gpsFix = uartingps[4];
	  
	}
	/* UBX NAV-VELNED */
	if (class == 0x01 && id == 0x12) {
	  
	  velN = (int32_t)(uartingps[4] + (uartingps[5] << 8) + (uartingps[6] << 16) + (uartingps[7] << 24));
	  velE = (int32_t)(uartingps[8] + (uartingps[9] << 8) + (uartingps[10] << 16) + (uartingps[11] << 24));
	  velD = (int32_t)(uartingps[12] + (uartingps[13] << 8) + (uartingps[14] << 16) + (uartingps[15] << 24));
	  speed = (uint32_t)(uartingps[16] + (uartingps[17] << 8) + (uartingps[18] << 16) + (uartingps[19] << 24));
	  gspeed = (uint32_t)(uartingps[20] + (uartingps[21] << 8) + (uartingps[22] << 16) + (uartingps[23] << 24));
	  heading = (int32_t)(uartingps[24] + (uartingps[25] << 8) + (uartingps[26] << 16) + (uartingps[27] << 24));
	  sAcc = (uint32_t)(uartingps[28] + (uartingps[29] << 8) + (uartingps[30] << 16) + (uartingps[31] << 24));
	  cAcc = (uint32_t)(uartingps[32] + (uartingps[33] << 8) + (uartingps[34] << 16) + (uartingps[35] << 24));

	}
      }
      break;   
  }
}


static const UARTConfig UARTpcfggps = {
  NULL,
  NULL,
  NULL,
  uartfun,
  NULL,
  /* HW dependent part.*/
  38400,
  0,
  0,
  0
};

static const UARTConfig UARTpcfgrf = {
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  /* HW dependent part.*/
  115200,
  0,
  0,
  0
};

icucnt_t last_width, last_period;

static void icuwidthcb(ICUDriver *icup) {

  last_width = icuGetWidth(icup);
}

static void icuperiodcb(ICUDriver *icup) {

  last_period = icuGetPeriod(icup);
}

static ICUConfig icucfg = {
  ICU_INPUT_ACTIVE_HIGH,
  10000,                                    /* 10kHz ICU clock frequency.   */
  icuwidthcb,
  icuperiodcb,
  NULL,
  ICU_CHANNEL_1
};

/*
 * IMU configuration (1.312500MHz, CPHA=1, CPOL=1, MSb first, DFF=1(16-bit data frame format)).
 */
static const SPIConfig ps_spicfg = {
  NULL,
  GPIOB,
  12,
  SPI_CR1_BR_2 | SPI_CR1_BR_0 | SPI_CR1_CPHA | SPI_CR1_CPOL | SPI_CR1_DFF
};

/*
 * SERIAL Device configuration for RF.
 * BitRate:     57600.
 */
static SerialConfig SERIALrfcfg = {
  57600,
  0,
  USART_CR2_STOP1_BITS | USART_CR2_LINEN,
  0
};

/*
 * SERIAL Device configuration for RF.
 * BitRate:     57600.
 */
static SerialConfig SERIALtestcfg = {
  57600,
  0,
  USART_CR2_STOP1_BITS | USART_CR2_LINEN,
  0
};


/*
 * Working area for the LED flashing thread.
 */
static WORKING_AREA(myThreadWorkingArea, 128);
 

__attribute__((noreturn))
static msg_t myThread(void *arg) {
  (void)arg;
  chRegSetThreadName("radio");
  EventListener elRadio;
  chEvtRegisterMask((EventSource *)chnGetEventSource(&SD5), &elRadio, 1);
  while (TRUE) {
    flagsmask_t flags;
    chEvtWaitOne(1);

    chSysLock();
    flags = chEvtGetAndClearFlagsI(&elRadio);
    chSysUnlock();
    chprintf((BaseSequentialStream *)&SD3, "Flags %d\r\n", 
	    (uint16_t)flags);
    if (flags & CHN_INPUT_AVAILABLE) {
      palTogglePad(GPIOD, GPIOD_LED);
      int8_t c = 0;
      while (c != Q_TIMEOUT) {
	inputsd[0] = (uint8_t)c;
	c = chnGetTimeout(&SD5, TIME_IMMEDIATE);
      }
    }
}
    
//     chEvtWaitOneTimeout(EVENT_MASK(1), MS2ST(10));
//     chSysLock();
//     flags = chEvtGetAndClearFlags(&s5EventListener);
//     chSysUnlock();
//     
//     if (flags & CHN_INPUT_AVAILABLE) {
//       uint8_t c;
//       c = chnGetTimeout(&SD5, TIME_IMMEDIATE);
//     }
   

//       inputsdsize = sdReadTimeout(&SD5, inputsd, 16, 100);
//       if (inputsdsize > 0) {
// 	palTogglePad(GPIOD, GPIOD_LED);
//       }

//     timer += MS2ST(1000);            // Next deadline
//     chThdSleepUntil(timer);
//   }
  
}

static uint8_t position;

void unASpacketer_uint8_t(uint8_t * buffer, uint8_t * input, uint8_t inc){
  if(inc) {
    memcpy(buffer+position, (const uint8_t *)input, 1);
    position++;
  }
  else {
    memcpy(buffer, (const uint8_t *)input, 1);  
    position = 1;

  }
  
}

void unASpacketer_uint16_t(uint8_t * buffer, uint16_t * input, uint8_t inc){
  if(inc) {
    memcpy(buffer+position, (const uint8_t *)input, 2);
    position+=2;
  }
  else {
    memcpy(buffer, (const uint8_t *)input, 2);  
    position = 1;

  }
  
}

void unASpacketer_uint32_t(uint8_t * buffer, uint32_t * input, uint8_t inc){
  if(inc) {
    memcpy(buffer+position, (const uint8_t *)input, 4);
    position+=4;
  }
  else {
    memcpy(buffer, (const uint8_t *)input, 4);  
    position = 1;

  }
  
}

void unASpacketer_int32_t(uint8_t * buffer, int32_t * input, uint8_t inc){
  if(inc) {
    memcpy(buffer+position, (const uint8_t *)input, 4);
    position+=4;
  }
  else {
    memcpy(buffer, (const uint8_t *)input, 4);  
    position = 1;

  }
  
}

void unASpacketer_float(uint8_t * buffer, float * input, uint8_t inc){
  if(inc) {
    memcpy(buffer+position, (const uint8_t *)input, 4);
    position+=4;
  }
  else {
    memcpy(buffer, (const uint8_t *)input, 4);  
    position = 1;

  }
  
}


/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 9, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOA, 10, PAL_MODE_INPUT);				/* Rx.     */
 
  palSetPadMode(GPIOC, 10, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOC, 11, PAL_MODE_INPUT);				/* Rx.     */
  
  uartStart(&UARTD2,&UARTpcfggps);
  palSetPadMode(GPIOA, 2, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOA, 3, PAL_MODE_INPUT);				/* Rx.     */
  
//   uartStart(&UARTD3,&UARTpcfgrf);
  sdStart(&SD3, &SERIALrfcfg);
  palSetPadMode(GPIOC, 10, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOC, 11, PAL_MODE_INPUT);				/* Rx.     */
 
  sdStart(&SD5, &SERIALtestcfg);
  palSetPadMode(GPIOC, 12, PAL_MODE_STM32_ALTERNATE_PUSHPULL);		/* Tx.     */
  palSetPadMode(GPIOD, 2, PAL_MODE_INPUT);				/* Rx.     */
   /*
  * PWM2, PWM_OUT 2 a 4.
  */
  pwmStart(&PWMD2, &pwmcfga);
  palSetPadMode(GPIOA, 1, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* Channel 2 */
  palSetPadMode(GPIOB, 10, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* Channel 3 */
  palSetPadMode(GPIOB, 11, PAL_MODE_STM32_ALTERNATE_PUSHPULL);	/* Channel 4 */
  
  /*
  * PWM1, U15.
  */
  pwmStart(&PWMD1, &pwmcfgb);
  palSetPadMode(GPIOA, 8, PAL_MODE_STM32_ALTERNATE_PUSHPULL);       /* C2M4.   Channel 1*/
  palSetPadMode(GPIOA, 11, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M4.   Channel 4 */
  palClearPad(GPIOA, 11);

  /*
  * PWM3, U14 y U12.
  */
  pwmStart(&PWMD3, &pwmcfgc);
  palSetPadMode(GPIOC, 6, PAL_MODE_STM32_ALTERNATE_PUSHPULL);      /* C2M3.   Channel 1 */
  palSetPadMode(GPIOC, 7, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M3.   Channel 2 */
  palClearPad(GPIOC, 7);
  
  palSetPadMode(GPIOC, 8, PAL_MODE_STM32_ALTERNATE_PUSHPULL);      /* C2M1.   Channel 3 */
  palSetPadMode(GPIOC, 9, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M1.   Channel 4 */  
  palClearPad(GPIOC, 9);

  /*
  * PWM4, U13.
  */
  pwmStart(&PWMD4, &pwmcfgd);
  palSetPadMode(GPIOB, 9, PAL_MODE_STM32_ALTERNATE_PUSHPULL);      /* C2M2.   Channel 4 */   
  palSetPadMode(GPIOB, 8, PAL_MODE_OUTPUT_PUSHPULL);      /* C1M2.   Channel 3 */
  palClearPad(GPIOB, 8);


  pwmEnableChannel(&PWMD2, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
  pwmEnableChannel(&PWMD2, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
  pwmEnableChannel(&PWMD2, 3, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  pwmEnableChannel(&PWMD1, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  pwmEnableChannel(&PWMD3, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 
  pwmEnableChannel(&PWMD3, 2, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  pwmEnableChannel(&PWMD4, 3, PWM_PERCENTAGE_TO_WIDTH(&PWMD2, 7500)); 

  /* ICU PWM Measure Pin. */
  palSetPadMode(GPIOA, 0, PAL_MODE_INPUT);
  icuStart(&ICUD5, &icucfg);
  icuEnable(&ICUD5);
  
  /* ADC Group of Servo Measurements*/
  palSetGroupMode(GPIOC, PAL_PORT_BIT(0) | PAL_PORT_BIT(1) | PAL_PORT_BIT(2) | 
			 PAL_PORT_BIT(3) | PAL_PORT_BIT(4) | PAL_PORT_BIT(5),
                  0, PAL_MODE_INPUT_ANALOG);
  /* Preasure Sensor Measure Pin. */
  palSetPadMode(GPIOB, 1, PAL_MODE_INPUT_ANALOG);
  adcStart(&ADCD1, NULL);
  
  /* IMU SPI Pins. */
  palSetPadMode(GPIOB, 13, PAL_MODE_STM32_ALTERNATE_PUSHPULL);     /* SCK. */
  palSetPadMode(GPIOB, 14, PAL_MODE_STM32_ALTERNATE_PUSHPULL);     /* MISO.*/
  palSetPadMode(GPIOB, 15, PAL_MODE_STM32_ALTERNATE_PUSHPULL);     /* MOSI.*/
  palSetPadMode(GPIOB, 12, PAL_MODE_OUTPUT_PUSHPULL);		   /* NS. */
  palSetPad(GPIOB, 12);
  spiAcquireBus(&SPID2);              /* Acquire ownership of the bus.    */
  spiStart(&SPID2, &ps_spicfg);       /* Setup transfer parameters.       */
  spiSelect(&SPID2);                  /* Slave Select assertion.          */
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
//     palSetPadMode(GPIOD, GPIOD_LED, PAL_MODE_OUTPUT_PUSHPULL);

  /* Starting the flashing LEDs thread.*/
  (void)chThdCreateStatic(myThreadWorkingArea, sizeof(myThreadWorkingArea),
			  NORMALPRIO, myThread, NULL);
 
  /* Kalman Filter initialization. */
  unas_kalman_V2_initialize();

  /* MAVLINK protocol initialization. */
  mavlinkSystem.sysid = 1;                   ///< ID 1 for this airplane
  mavlinkSystem.compid = MAV_COMP_ID_SYSTEM_CONTROL;     ///< The component sending the message is the IMU, it could be also a Linux process
  mavlinkSystem.type = MAV_TYPE_GROUND_ROVER;   ///< This system is an airplane / fixed wing

  while (TRUE) {
    time = chTimeNow();
    time += MS2ST(delay);            // Next deadline
    
    adcConvert(&ADCD1, &adcgrpcfg1, samples1, ADC_GRP1_BUF_DEPTH);
    adcConvert(&ADCD1, &adcgrpcfg2, samples2, ADC_GRP2_BUF_DEPTH);
    
//     spiExchange(&SPID2, 1, imutxbuf, imurxbuf);          /* Atomic transfer operations.      */
  
    spiSelect(&SPID2);                  /* Slave Select assertion.          */
    spiReceive(&SPID2, 14, burstrxbuf);          /* Atomic transfer operations.      */
    spiSend(&SPID2, 1, bursttxbuf);          /* Atomic transfer operations.      */
    spiUnselect(&SPID2);                  /* Slave Select assertion.          */
    
//     palTogglePad(GPIOD, GPIOD_LED);
//     chprintf((BaseSequentialStream *)&SD1, "Hola desde SIEBOT2 Serial1.\r\nICU %u %u \r\nADC %u %u %u %u %u %u\r\nPREASSURE %u\r\nGPS %u %u %u %d %d %d %d %u %u\r\nBURST READ %X %X %X %X %X %X %X %X %X %X %X %X %X %X %X\r\n", 
// 	     last_width, last_period, 
// 	     samples1[0], samples1[1], samples1[2], samples1[3], samples1[4], samples1[5], 
// 	     samples2[0],
// 	     ck_a, ck_b, iTOW, lon, lat, height, hMSL, hAcc, vAcc, 
// 	     burstrxbuf[0], burstrxbuf[1], burstrxbuf[2], burstrxbuf[3], 
// 	     burstrxbuf[4], burstrxbuf[5], burstrxbuf[6], burstrxbuf[7], 
// 	     burstrxbuf[8], burstrxbuf[9], burstrxbuf[10], burstrxbuf[11], 
// 	     burstrxbuf[12], burstrxbuf[13], imurxbuf[0]);
    
    /* Test Dump Stream 1*/
//     chprintf((BaseSequentialStream *)&SD1, "S %u %u %u %u %u %u %u %u %u %u %u %u %d %d %d %d %u %u %X %X %X %X %X %X %X %X %X %X %X %X %X %X\r\n", 
// 	  last_width, last_period, 
// 	  samples1[0], samples1[1], samples1[2], samples1[3], samples1[4], samples1[5], 
// 	  samples2[0],
// 	  ck_a, ck_b, iTOW, lon, lat, height, hMSL, hAcc, vAcc, 
// 	  burstrxbuf[0], burstrxbuf[1], burstrxbuf[2], burstrxbuf[3], 
// 	  burstrxbuf[4], burstrxbuf[5], burstrxbuf[6], burstrxbuf[7], 
// 	  burstrxbuf[8], burstrxbuf[9], burstrxbuf[10], burstrxbuf[11], 
// 	  burstrxbuf[12], burstrxbuf[13]);
    
    gx = (burstrxbuf[1]&0x2000)>1?((int16_t)((~(burstrxbuf[1]|0xC000))+1)*-0.05):((burstrxbuf[1]&0x3FFF)*0.05);
    gy = (burstrxbuf[2]&0x2000)>1?((int16_t)((~(burstrxbuf[2]|0xC000))+1)*-0.05):((burstrxbuf[2]&0x3FFF)*0.05);
    gz = (burstrxbuf[3]&0x2000)>1?((int16_t)((~(burstrxbuf[3]|0xC000))+1)*-0.05):((burstrxbuf[3]&0x3FFF)*0.05);
    ax = (burstrxbuf[4]&0x2000)>1?((int16_t)((~(burstrxbuf[4]|0xC000))+1)*-3.333):((burstrxbuf[4]&0x3FFF)*3.333);
    ay = (burstrxbuf[5]&0x2000)>1?((int16_t)((~(burstrxbuf[5]|0xC000))+1)*-3.333):((burstrxbuf[5]&0x3FFF)*3.333);
    az = (burstrxbuf[6]&0x2000)>1?((int16_t)((~(burstrxbuf[6]|0xC000))+1)*-3.333):((burstrxbuf[6]&0x3FFF)*3.333);
//     mx = (burstrxbuf[7]&0x2000)>1?((int16_t)((~(burstrxbuf[7]|0xC000))+1)*-0.5):((burstrxbuf[7]&0x3FFF)*0.5);
//     my = (burstrxbuf[8]&0x2000)>1?((int16_t)((~(burstrxbuf[8]|0xC000))+1)*-0.5):((burstrxbuf[8]&0x3FFF)*0.5);
//     mz = (burstrxbuf[9]&0x2000)>1?((int16_t)((~(burstrxbuf[9]|0xC000))+1)*-0.5):((burstrxbuf[9]&0x3FFF)*0.5);
   
    kalman_v2_in[0] = gx;
    kalman_v2_in[1] = gy;
    kalman_v2_in[2] = gz;
    kalman_v2_in[3] = ax;
    kalman_v2_in[4] = ay;
    kalman_v2_in[5] = az;

    unas_kalman_V2(kalman_v2_in, kalman_v2_out);

    // Prueba 2 memcpy
    unASpacketer_uint8_t(prueba, &prueba_header[0], FALSE);
    unASpacketer_uint8_t(prueba, &prueba_header[1], TRUE);
    unASpacketer_uint8_t(prueba, &prueba_header[2], TRUE);
    // Pressure data packaging.
    unASpacketer_uint16_t(prueba, &samples2[0], TRUE);
    
    // GPS data packaging.
    unASpacketer_uint32_t(prueba, &iTOW, TRUE);
    unASpacketer_int32_t(prueba, &lon, TRUE);
    unASpacketer_int32_t(prueba, &lat, TRUE);
    unASpacketer_int32_t(prueba, &height, TRUE);
    unASpacketer_uint32_t(prueba, &hAcc, TRUE);
    unASpacketer_uint32_t(prueba, &vAcc, TRUE);
    unASpacketer_uint8_t(prueba, &gpsFix, TRUE);
    unASpacketer_int32_t(prueba, &velN, TRUE);
    unASpacketer_int32_t(prueba, &velE, TRUE);
    unASpacketer_int32_t(prueba, &velD, TRUE);
    unASpacketer_uint32_t(prueba, &speed, TRUE);
    unASpacketer_uint32_t(prueba, &gspeed, TRUE);
    unASpacketer_int32_t(prueba, &heading, TRUE);
    unASpacketer_uint32_t(prueba, &sAcc, TRUE);
    unASpacketer_uint32_t(prueba, &cAcc, TRUE);
    
    // IMU data packaging.
    unASpacketer_uint16_t(prueba, &burstrxbuf[0], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[1], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[2], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[3], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[4], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[5], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[6], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[7], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[8], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[9], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[10], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[11], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[12], TRUE);
    unASpacketer_uint16_t(prueba, &burstrxbuf[13], TRUE);
    
    // Kalman output data packaging.
    unASpacketer_float(prueba, &kalman_v2_out[0], TRUE);
    unASpacketer_float(prueba, &kalman_v2_out[1], TRUE);
    unASpacketer_float(prueba, &kalman_v2_out[2], TRUE);
     
    unASpacketer_uint8_t(prueba, &prueba_header[3], TRUE);
    
//     chprintf((BaseSequentialStream *)&SD1, "SOF\n");
//     chSequentialStreamWrite((BaseSequentialStream *)&SD1, prueba, 103);
//     chprintf((BaseSequentialStream *)&SD1, "\n");
    /* Test Dump Stream 2*/
    chprintf((BaseSequentialStream *)&SD1, "S %u %u %u %u %u %u %d %d %d %d %u %u %d %d %d %u %u %d %u %u %X %X %X %X %X %X %X %X %X %X %X %X %X %X %f %f %f\r\n", 
	  samples2[0],
	  samples1[0], samples1[1], samples1[2], 
	  last_width,
	  iTOW, lon, lat, height, hAcc, vAcc, gpsFix, velN, velE, velD, speed, gspeed, heading, sAcc, cAcc,
	  burstrxbuf[0], burstrxbuf[1], burstrxbuf[2], burstrxbuf[3], 
	  burstrxbuf[4], burstrxbuf[5], burstrxbuf[6], burstrxbuf[7], 
	  burstrxbuf[8], burstrxbuf[9], burstrxbuf[10], burstrxbuf[11], 
	  burstrxbuf[12], burstrxbuf[13], 
	  kalman_v2_out[0], kalman_v2_out[1], kalman_v2_out[2]);

    /* RF transmition. */
    tlcounter +=1;
    
    if(tlcounter == 50) {
      
//       chprintf((BaseSequentialStream *)&SD3, "S %u\r\n", 
// 	    time);
//       chprintf((BaseSequentialStream *)&SD3, 
// 	       "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890\r\n");
//       chprintf((BaseSequentialStream *)&SD5, "S %u\r\n", 
// 	    time);
      /* HEARTBEAT ( #0 ) */
      mavlink_msg_heartbeat_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &mavlinkMsg, mavlinkSystemType, mavlinkAutopilotType, mavlinkSystemMode, mavlinkCustomMode, mavlinkSystemState);
      mavlinkLength = mavlink_msg_to_send_buffer(mavlinkBuffer, &mavlinkMsg);
      chSequentialStreamWrite((BaseSequentialStream *)&SD5, mavlinkBuffer, mavlinkLength);
    
      /* GPS_RAW_INT ( #24 )*/
      mavlink_msg_gps_raw_int_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &mavlinkMsg,
						       iTOW, gpsFix, lat, lon, hMSL, 65535, 65535, (uint16_t)(gspeed*10), 65535, 255);
      mavlinkLength = mavlink_msg_to_send_buffer(mavlinkBuffer, &mavlinkMsg);
      chSequentialStreamWrite((BaseSequentialStream *)&SD5, mavlinkBuffer, mavlinkLength);

      /* ATTITUDE ( #30 )*/
//       mavlink_msg_attitude_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &mavlinkMsg,
// 						       uint32_t time_boot_ms, float roll, float pitch, float yaw, float rollspeed, float pitchspeed, float yawspeed);
//       mavlinkLength = mavlink_msg_to_send_buffer(mavlinkBuffer, &mavlinkMsg);
//       chSequentialStreamWrite((BaseSequentialStream *)&SD5, mavlinkBuffer, mavlinkLength);

      tlcounter = 0;
	    
    }
    
    chThdSleepUntil(time);
//     chThdSleepMilliseconds(500);
  }
}
