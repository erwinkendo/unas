/*
    unAS - Copyright (C) 2013 Erwin Lopez.

    This file is part of unAS.

    unAS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    unAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    obhbridge.h
 * @brief   SIE2 On Board H-Bridge interface module through PWM header.
 *
 * @addtogroup obhbridge
 * @{
 */

#ifndef _OBHBRIDGE_H_
#define _OBHBRIDGE_H_

#if HAL_USE_PWM || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/**
 * @name    On Board H-Bridge outputs
 * @{
 */
#define obhbridge1                 1
#define obhbridge2                 2
#define obhbridge3                 3
#define obhbridge4                 4
/** @} */

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
  void obhbridgeTurnCW(uint8_t output, uint16_t width);
  void obhbridgeTurnCCW(uint8_t output, uint16_t width);
  void obhbridgeStop(uint8_t output);
  void obhbridgeSetPWM(uint8_t output, uint16_t width);
#ifdef __cplusplus
}
#endif

#endif /* HAL_USE_PWM */ 

#endif/* _OBHBRIDGE_H_ */

/** @} */
