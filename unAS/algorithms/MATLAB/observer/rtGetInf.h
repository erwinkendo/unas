/*
 * rtGetInf.h
 *
 * Code generation for function 'observer'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

#ifndef __RTGETINF_H__
#define __RTGETINF_H__

#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"

extern real_T rtGetInf(void);
extern real32_T rtGetInfF(void);
extern real_T rtGetMinusInf(void);
extern real32_T rtGetMinusInfF(void);

#endif
/* End of code generation (rtGetInf.h) */
