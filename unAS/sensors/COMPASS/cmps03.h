/**
 * @file    cmps03.h
 * @brief   CMPS03 Digital Compass interface module through I2C header.
 *
 * @addtogroup chr6d
 * @{
 */

#ifndef _CMPS03_H_
#define _CMPS03_H_

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

#define cmps03Adrr0                  0xC0
#define cmps03Adrr2                  0xC2
#define cmps03Adrr4                  0xC4
#define cmps03Adrr6                  0xC6
#define cmps03Adrr8                  0xC8
#define cmps03AdrrA                  0xCA
#define cmps03AdrrC                  0xCC
#define cmps03AdrrE                  0xCE

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

static const I2CConfig cmps03I2Cpcfg;

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
  void cmps03RequestData(I2CDriver *i2cp, uint8_t a, uint8_t addr);
  uint8_t cmps03GetHeading(void);
  uint16_t cmps03GetHeadingf(void);
#ifdef __cplusplus
}
#endif

#endif /* _CMPS03_H_ */

/** @} */
