/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"

#define ADC_NUM_CHANNELS   1
#define ADC_BUF_DEPTH      1

static adcsample_t samples[ADC_NUM_CHANNELS * ADC_BUF_DEPTH];





/*
 * ADC conversion group.
 * Mode:        Continuous, 1 samples of 1 channels, SW triggered.
 * Channels:    IN1.
 */
static const ADCConversionGroup adcgrpcfg5 = {
  TRUE,
  ADC_NUM_CHANNELS,
  NULL,
  NULL,
  0,                                 /* CR1 */
  ADC_CR2_SWSTART,                   /* CR2 */
  0,                                 /* SMPR1 */
  ADC_SMPR2_SMP_AN1(ADC_SAMPLE_56), /* SMPR2 */
  ADC_SQR1_NUM_CH(ADC_NUM_CHANNELS), /* SQR1 */
  0,                                 /* SQR2 */
  ADC_SQR3_SQ1_N(ADC_CHANNEL_IN1)    /* SQR3 */
};



/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();



    /*
   * Initializes the ADC driver 1.
   * The pin PA1 on the port GPIOA is programmed as analog input.
   */
  adcStart(&ADCD1, NULL);
  palSetPadMode(GPIOA, 1, PAL_MODE_INPUT_ANALOG);
  
  /*
   * Starts an ADC continuous conversion.
   */
  adcStartConversion(&ADCD1, &adcgrpcfg5, samples, ADC_BUF_DEPTH);
  

  
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));
  

  while (TRUE) {
    

      

    chprintf((BaseSequentialStream *)&SD2, "Voltaje: %u \r\n", samples[0]);
   chThdSleepMilliseconds(1000);
  }
}
