/**
 * @file    lea6h.c
 * @brief   uBlox LEA6H GPS interface module through UART code.
 *
 * @addtogroup lea6h
 * @{
 */

#if HAL_USE_UART || defined(__DOXYGEN__)

#include "ch.h"
#include "hal.h"
#include "lea6h.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/
/**
 * @brief    uBlox LEA6H GPS UART configuration structure
 */
static const UARTConfig lea6hUARTpcfg = {
  NULL,
  NULL,
  NULL,
  lea6hUARTFun,
  NULL,
  /* HW dependent part.*/
  38400,
  0,
  0,
  0
};

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

static uint8_t uartingps[180];
static uint16_t flag = 0;
static uint16_t length = 0;
static uint8_t plcounter = 0;


static uint8_t class = 0;
static uint8_t id = 0;
static uint8_t ck_a = 0;
static uint8_t ck_b = 0;
static uint32_t iTOW = 0;
static int32_t lon = 0;
static int32_t lat = 0;
static int32_t height = 0;
static int32_t hMSL = 0;
static uint32_t hAcc = 0;
static uint32_t vAcc = 0;
static uint8_t gpsFix = 0;
static int32_t velN = 0;
static int32_t velE = 0;
static int32_t velD = 0;
static uint32_t speed = 0;
static uint32_t gspeed = 0;
static int32_t heading = 0;
static uint32_t sAcc = 0;
static uint32_t cAcc = 0;

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief    uBlox LEA6H GPS reception callback function
 */
void lea6hUARTFun(UARTDriver *uartp, uint16_t c) {
  (void) uartp;

  /* Initial GPS Decoding condition, no previous operation done. */
  switch(flag) {
    case 0:
      if (c == 0xB5) {   
	flag ++;
      }
      break;
    case 1:
      if (c == 0X62) { 
	flag ++;	
      }
      break;
    case 2:
      class = c;
      ck_b = ck_a = c;
      flag ++;
      break;
    case 3:
      id = c;
      ck_b += (ck_a += c); 
      flag ++;
      break;
    case 4:
      length = c;
      ck_b += (ck_a += c); 
      flag ++;
      break;
    case 5:
      length += (uint16_t)(c << 8);
      ck_b += (ck_a += c);
      flag ++;
      if (length > 512) {
	flag = 0;
      }
      plcounter = 0;
      break;
    case 6:
      ck_b += (ck_a += c);
      uartingps[plcounter] = c;
      if (++plcounter == length) {
	flag ++;	
      }
      break;
    case 7:
      flag ++;
      if (ck_a != c) {
	flag = 0;
      }
      break;
    case 8:
      flag = 0;
      if (ck_b == c) {
	/* UBX NAV-POSLLH */
	if (class == 0x01 && id == 0x02) {
	  
	  iTOW = (uint32_t)(uartingps[0] + (uartingps[1] << 8) + (uartingps[2] << 16) + (uartingps[3] << 24));
	  lon = (int32_t)(uartingps[4] | (uartingps[5] << 8) | (uartingps[6] << 16) | (uartingps[7] << 24));
	  lat = (int32_t)(uartingps[8] | (uartingps[9] << 8) | (uartingps[10] << 16) | (uartingps[11] << 24));
	  height = (int32_t)(uartingps[12] + (uartingps[13] << 8) + (uartingps[14] << 16) + (uartingps[15] << 24));
	  hMSL = (int32_t)(uartingps[16] + (uartingps[17] << 8) + (uartingps[18] << 16) + (uartingps[19] << 24));
	  hAcc = (uint32_t)(uartingps[20] + (uartingps[21] << 8) + (uartingps[22] << 16) + (uartingps[23] << 24));
	  vAcc = (uint32_t)(uartingps[24] + (uartingps[25] << 8) + (uartingps[26] << 16) + (uartingps[27] << 24));
	}
	/* UBX NAV-STATUS */
	if (class == 0x01 && id == 0x03) {
	  
	  gpsFix = uartingps[4];
	  
	}
	/* UBX NAV-VELNED */
	if (class == 0x01 && id == 0x12) {
	  
	  velN = (int32_t)(uartingps[4] + (uartingps[5] << 8) + (uartingps[6] << 16) + (uartingps[7] << 24));
	  velE = (int32_t)(uartingps[8] + (uartingps[9] << 8) + (uartingps[10] << 16) + (uartingps[11] << 24));
	  velD = (int32_t)(uartingps[12] + (uartingps[13] << 8) + (uartingps[14] << 16) + (uartingps[15] << 24));
	  speed = (uint32_t)(uartingps[16] + (uartingps[17] << 8) + (uartingps[18] << 16) + (uartingps[19] << 24));
	  gspeed = (uint32_t)(uartingps[20] + (uartingps[21] << 8) + (uartingps[22] << 16) + (uartingps[23] << 24));
	  heading = (int32_t)(uartingps[24] + (uartingps[25] << 8) + (uartingps[26] << 16) + (uartingps[27] << 24));
	  sAcc = (uint32_t)(uartingps[28] + (uartingps[29] << 8) + (uartingps[30] << 16) + (uartingps[31] << 24));
	  cAcc = (uint32_t)(uartingps[32] + (uartingps[33] << 8) + (uartingps[34] << 16) + (uartingps[35] << 24));

	}
      }
      break;   
  }
}


/**
 * @brief   Reads a register value.
 * @pre     The SPI interface must be initialized and the driver started.
 *
 * @param[in] spip      pointer to the SPI initerface
 * @param[in] reg       register number
 * @return              The register value.
 */

uint32_t lea6hGetTime(void) {
  
  return iTOW;
  
}

int32_t lea6hGetLatitude(void) {
  
  return lat;
  
}

int32_t lea6hGetLongitude(void) {
  
  return lon;
  
}

int32_t lea6hGetHeight(void) {
  
  return height;
  
}

int32_t lea6hGetHeightMSL(void) {
  
  return height;
  
}

uint32_t lea6hGetHAcc(void) {
  
  return hAcc;
  
}

uint32_t lea6hGetVAcc(void) {

  return vAcc;
  
}

#endif /* HAL_USE_UART */

/** @} */
