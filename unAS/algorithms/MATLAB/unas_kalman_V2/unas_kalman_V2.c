/*
 * unas_kalman_V2.c
 *
 * Code generation for function 'unas_kalman_V2'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "unas_kalman_V2.h"
#include "inv.h"
#include "unas_kalman_V2_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */
  real_T y[3];
  real_T sin_phi;
  real_T cos_phi;
  real_T cos_theta;
  real_T tan_theta;
  real_T omega[3];
  int32_T i;
  real_T bias[3];
  int32_T i0;
  static const int8_T a[18] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
    0, 1 };

  real_T omega_r[3];
  real_T b[9];
  real_T b_tan_theta[36];
  real_T A[36];
  static const int8_T iv0[6] = { 0, 0, 0, 1, 0, 0 };

  static const int8_T iv1[6] = { 0, 0, 0, 0, 1, 0 };

  static const int8_T iv2[6] = { 0, 0, 0, 0, 0, 1 };

  real_T x_m[6];
  int32_T i1;
  real_T P_m[36];
  real_T d0;
  static const real_T Q[36] = { 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1 };

  real_T b_x_pre[6];
  real_T b_b[18];
  real_T c_b[18];
  static const real_T R[9] = { 0.2, 0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.2 };

  real_T d_b[9];
  real_T K[18];
  real_T b_y[3];
  real_T sin_theta[3];
  real_T dv0[3];

/* Variable Definitions */

/* Function Declarations */
static real_T rt_powd_snf(real_T u0, real_T u1);

/* Function Definitions */
static real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T d1;
  real_T d2;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    d1 = fabs(u0);
    d2 = fabs(u1);
    if (rtIsInf(u1)) {
      if (d1 == 1.0) {
        y = rtNaN;
      } else if (d1 > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d2 == 0.0) {
      y = 1.0;
    } else if (d2 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void unas_kalman_V2(const real_T z[6], real_T out[3])
{

  /* % */
  /* /$***************************************************************************** */
  /*  * FILE: unas_kalman.c */
  /*  * DESCRIPTION: */
  /*  * Kalman filter to estimate rotation angles \theta,\phi,\psi based on */
  /*  * gyroscope measuremnts for estimation and accelerometers for error propagation */
  /*  * */
  /*  * */
  /*  * Universidad Nacional de Colombia */
  /*  * Ingenieria Mecanica y Mecatronica */
  /*  * Copyright 2013 Universidad Nacional de Colombia. All rights reserved. */
  /*  * */
  /*  *****************************************************************************$/ */
  /* ************************************************************************** */
  /*  Define constant variable and conversion constants */
  /*  */
  /*  */
  /*  */
  /* ************************************************************************** */
  /* addpath libraries */
  /*  [n,m] = size(z); % Size of data */
  /*  Converion rad to degrees and scaling factor for  gyros */
  /*  Sampling time */
  /*  System variance */
  /*  Sensor variance */
  /* ************************************************************************** */
  /*  Initialize system: */
  /*  variable x_pre is an estimate of the estimation error due to bias */
  /*  in gyroscopic measuremnets. The first 3 components of the x_pre vector */
  /*  are \delta(angle) and the last three correspond to \delta{angular velocity} */
  /*  */
  /*  */
  /* ************************************************************************** */
  /*  phi_pre   = 0; % angular variable */
  /*  theta_pre = 0; */
  /*  psi_pre   = 0; */
  /* variable container to plot output errors x_pre */
  /* ************************************************************************** */
  /*  Get IMU measurment data */
  /*  */
  /*  */
  /*  */
  /* ************************************************************************** */
  /*  Remove this declaration for direct assignment */
  /*  theta_est = zeros(1); */
  /*  phi_est = zeros(1); */
  /*  psi_est = zeros(1); */
  /* ************************************************************************** */
  /*  Main EKF loop: */
  /*  */
  /*  */
  /*  */
  /* ************************************************************************** */
  /*  for i = 1:n */
  /*  Measure velocity vector */
  y[0] = z[0] * 0.017458333333333333;
  y[1] = -z[1] * 0.017458333333333333;
  y[2] = -z[2] * 0.017458333333333333;

  /* [w_x ; w_y ; w_z] */
  /*  Measurement ACCEL vector */
  /* [a_x;a_y;a_z] */
  /* ****************************************** */
  /*  Define EKF functions */
  /*  */
  /* ****************************************** */
  /* ************** */
  /*  Estimation process using gyroscopic neasurments */
  /*  - The C*x_pre is the bias estimate, which is added to \omega, the */
  /*  velocity measuremes */
  /*  - The estimatin error due to bias is added */
  /*  */
  /* ************** */
  sin_phi = sin(phi_pre);
  cos_phi = cos(phi_pre);
  cos_theta = cos(theta_pre);
  tan_theta = tan(theta_pre);
  for (i = 0; i < 3; i++) {
    omega[i] = y[i];

    /*  Select only the bias estimate C = [zeros(3,3) eye(3)]; */
    /*  Compute rotation matrix to NED frame */
    /*  Integration of the velocity */
    /*  incorporate bias estimate C*x_pre */
    /*  */
    bias[i] = 0.0;
    for (i0 = 0; i0 < 6; i0++) {
      bias[i] += (real_T)a[i + 3 * i0] * x_pre[i0];
    }
  }

  /*  Review this assignment */
  omega_r[0] = omega[0] + bias[0];
  omega_r[1] = omega[1] + bias[1];
  omega_r[2] = omega[2] + bias[2];
  b[0] = 1.0;
  b[3] = sin_phi * tan_theta;
  b[6] = cos_phi * tan_theta;
  b[1] = 0.0;
  b[4] = cos_phi;
  b[7] = -sin_phi;
  b[2] = 0.0;
  b[5] = sin_phi / cos_theta;
  b[8] = cos_phi / cos_theta;
  for (i = 0; i < 3; i++) {
    bias[i] = 0.0;
    for (i0 = 0; i0 < 3; i0++) {
      bias[i] += b[i + 3 * i0] * omega_r[i0];
    }
  }

  /*  Reduce error due to estimation Bias */
  /*  Linearization of the systema in order to obtain the transition matrix */
  /*  A as a first order Taylor series approximation  */
  /*  */
  b_tan_theta[0] = tan_theta * (cos_phi * omega[1] - sin_phi * omega[2]);
  b_tan_theta[6] = (1.0 + rt_powd_snf(tan_theta, 2.0)) * (sin_phi * omega[1] +
    cos_phi * omega[2]);
  b_tan_theta[12] = 0.0;
  b_tan_theta[18] = 1.0;
  b_tan_theta[24] = sin_phi * tan_theta;
  b_tan_theta[30] = cos_phi * tan_theta;
  b_tan_theta[1] = -(sin_phi * omega[1] + cos_phi * omega[2]);
  b_tan_theta[7] = 0.0;
  b_tan_theta[13] = 0.0;
  b_tan_theta[19] = 0.0;
  b_tan_theta[25] = cos_phi;
  b_tan_theta[31] = -sin_phi;
  b_tan_theta[2] = (cos_phi * omega[1] - sin_phi * omega[2]) / cos_theta;
  b_tan_theta[8] = sin(theta_pre) * (sin_phi * omega[1] + cos_phi * omega[2]) /
    rt_powd_snf(cos_theta, 2.0);
  b_tan_theta[14] = 0.0;
  b_tan_theta[20] = 0.0;
  b_tan_theta[26] = sin_phi / cos_theta;
  b_tan_theta[32] = cos_phi / cos_theta;
  for (i = 0; i < 6; i++) {
    b_tan_theta[3 + 6 * i] = (real_T)iv0[i];
    b_tan_theta[4 + 6 * i] = (real_T)iv1[i];
    b_tan_theta[5 + 6 * i] = (real_T)iv2[i];
    for (i0 = 0; i0 < 6; i0++) {
      A[i0 + 6 * i] = 0.02 * b_tan_theta[i0 + 6 * i];
    }
  }

  /*  Estimate the update of the error measurement matrix x_m = T*A*x_pre */
  /*  Solve Discrete Ricatti equation */
  /*  */
  /* Solve Ricatti P_m = A*P_pre*A' + Q; */
  for (i = 0; i < 6; i++) {
    x_m[i] = 0.0;
    for (i0 = 0; i0 < 6; i0++) {
      x_m[i] += 0.02 * A[i + 6 * i0] * x_pre[i0];
    }

    for (i0 = 0; i0 < 6; i0++) {
      b_tan_theta[i + 6 * i0] = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        b_tan_theta[i + 6 * i0] += A[i + 6 * i1] * P_pre[i1 + 6 * i0];
      }
    }
  }

  for (i = 0; i < 6; i++) {
    for (i0 = 0; i0 < 6; i0++) {
      d0 = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        d0 += b_tan_theta[i + 6 * i1] * A[i0 + 6 * i1];
      }

      P_m[i + 6 * i0] = d0 + Q[i + 6 * i0];
    }
  }

  theta_pre = (theta_pre + 0.02 * bias[1]) + x_pre[1];
  phi_pre = (phi_pre + 0.02 * bias[0]) + x_pre[0];
  psi_pre = (psi_pre + 0.02 * bias[2]) + x_pre[2];
  for (i = 0; i < 6; i++) {
    b_x_pre[i] = x_pre[i];
  }

  /* ************** */
  /*  Update process with error propagation using acceleration neasurments */
  /*  */
  /*  */
  /* ************** */
  sin_phi = sin(phi_pre);
  cos_phi = cos(phi_pre);
  tan_theta = sin(theta_pre);
  cos_theta = cos(theta_pre);

  /*  Linearize output matrix that transforms ACCEL frame to the static NED */
  /*  frame */
  /*  - the matrix z is the jacobian matrix of the nonlinear ouput function */
  b_b[0] = 0.0;
  b_b[3] = -cos_theta;
  b_b[6] = 0.0;
  b_b[9] = 0.0;
  b_b[12] = 0.0;
  b_b[15] = 0.0;
  b_b[1] = cos_theta * cos_phi;
  b_b[4] = -tan_theta * sin_phi;
  b_b[7] = 0.0;
  b_b[10] = 0.0;
  b_b[13] = 0.0;
  b_b[16] = 0.0;
  b_b[2] = -cos_theta * sin_phi;
  b_b[5] = -tan_theta * cos_phi;
  b_b[8] = 0.0;
  b_b[11] = 0.0;
  b_b[14] = 0.0;
  b_b[17] = 0.0;

  /*  Calculate new matrix gain K = P_m*z'*inv(z*P_m*z' + R) */
  for (i = 0; i < 3; i++) {
    for (i0 = 0; i0 < 6; i0++) {
      c_b[i + 3 * i0] = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        c_b[i + 3 * i0] += b_b[i + 3 * i1] * P_m[i1 + 6 * i0];
      }
    }
  }

  for (i = 0; i < 3; i++) {
    for (i0 = 0; i0 < 3; i0++) {
      d0 = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        d0 += c_b[i + 3 * i1] * b_b[i0 + 3 * i1];
      }

      b[i + 3 * i0] = d0 + R[i + 3 * i0];
    }
  }

  inv(b, d_b);
  for (i = 0; i < 6; i++) {
    for (i0 = 0; i0 < 3; i0++) {
      c_b[i + 6 * i0] = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        c_b[i + 6 * i0] += P_m[i + 6 * i1] * b_b[i0 + 3 * i1];
      }
    }

    for (i0 = 0; i0 < 3; i0++) {
      K[i + 6 * i0] = 0.0;
      for (i1 = 0; i1 < 3; i1++) {
        K[i + 6 * i0] += c_b[i + 6 * i1] * d_b[i1 + 3 * i0];
      }
    }
  }

  /*  Caluclate Error propagation asumming that the gravity is 1: */
  /*  - The estimated ouput is caculated as the multiplication of the */
  /*  output rotation matrix and the gravity vector [0 0 g]', and then */
  /*  adding the estimation error, i.e. y_est = R(\angle_pre)*[0 0 g]' + */
  /*  R(\angle_error)*[0 0 g]' */
  /*  -> x_pre = x_m + K*(ACCEL_MEASURE - y_est) */
  /*  Update matrix P_pre using new matrix gain K */
  /*  P_pre = (eye(6) - K*z)*P_m; */
  memset(&A[0], 0, 36U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    A[i + 6 * i] = 1.0;
  }

  for (i = 0; i < 6; i++) {
    for (i0 = 0; i0 < 6; i0++) {
      d0 = 0.0;
      for (i1 = 0; i1 < 3; i1++) {
        d0 += K[i + 6 * i1] * b_b[i1 + 3 * i0];
      }

      b_tan_theta[i + 6 * i0] = A[i + 6 * i0] - d0;
    }
  }

  for (i = 0; i < 6; i++) {
    for (i0 = 0; i0 < 6; i0++) {
      P_pre[i + 6 * i0] = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        P_pre[i + 6 * i0] += b_tan_theta[i + 6 * i1] * P_m[i1 + 6 * i0];
      }
    }
  }

  b_y[0] = 0.001 * z[3];
  b_y[1] = -0.001 * z[4];
  b_y[2] = -0.001 * z[5];
  sin_theta[0] = tan_theta;
  sin_theta[1] = -cos_theta * sin_phi;
  sin_theta[2] = -cos_theta * cos_phi;
  dv0[0] = sin(b_x_pre[1]);
  dv0[1] = -cos(b_x_pre[1]) * sin(b_x_pre[0]);
  dv0[2] = -cos(b_x_pre[1]) * cos(b_x_pre[0]);
  for (i = 0; i < 3; i++) {
    y[i] = (b_y[i] - sin_theta[i]) - dv0[i];
  }

  for (i = 0; i < 6; i++) {
    d0 = 0.0;
    for (i0 = 0; i0 < 3; i0++) {
      d0 += K[i + 6 * i0] * y[i0];
    }

    x_pre[i] = x_m[i] + d0;
  }

  /* ****************** */
  /*  store in vectors for plotting purposes */
  /*  */
  /* ****************** */
  /* theta_pre = theta_m; */
  /*  Record output */
  /*      y_est(i+1,:) = x_pre'; */
  /*      theta_est(i+1) = theta_pre; */
  /*      phi_est(i+1)   = phi_pre; */
  /*      psi_est(i+1)   = psi_pre; */
  /*      theta_est = theta_pre; */
  /*      phi_est = phi_pre; */
  /*      psi_est = psi_pre; */
  out[0] = theta_pre;
  out[1] = phi_pre;
  out[2] = psi_pre;

  /*  end */
  /* % */
  /*  figure(1) */
  /*  plot(theta_est/rad2grad) */
  /*   */
  /*  figure(2) */
  /*  plot(phi_est/rad2grad) */
  /*   */
  /*  figure(3) */
  /*  plot(psi_est/rad2grad) */
}

/* End of code generation (unas_kalman_V2.c) */
