/*
 * unas_kalman_V2_data.c
 *
 * Code generation for function 'unas_kalman_V2_data'
 *
 * C source code generated on: Mon Apr  1 16:24:26 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "unas_kalman_V2.h"
#include "unas_kalman_V2_data.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
real_T x_pre[6];
real_T P_pre[36];
real_T phi_pre;
real_T theta_pre;
real_T psi_pre;

/* Function Declarations */

/* Function Definitions */
/* End of code generation (unas_kalman_V2_data.c) */
