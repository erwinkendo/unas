/*
 * observer_terminate.c
 *
 * Code generation for function 'observer_terminate'
 *
 * C source code generated on: Fri Jun 14 13:33:07 2013
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "observer.h"
#include "observer_terminate.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
void observer_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (observer_terminate.c) */
