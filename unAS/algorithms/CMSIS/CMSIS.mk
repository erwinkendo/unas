# List of the available CMSIS (Cortex Microcontroller Software Interface
# Standard) for the Cortex-M processor series (M3, M4, M4 FP).

# Library for ARM-Cortex M3 series (little-endian), ARM compiler
CMSISM3SRC = $(CHIBIOS)/algorithms/CMSIS/arm_cortexM3l_math.lib
# Library for ARM-Cortex M4 series (FP Off, little-endian), ARM compiler
CMSISM4SRC = $(CHIBIOS)/algorithms/CMSIS/arm_cortexM4l_math.lib
# Library for ARM-Cortex M4 series, (FP On, little-endian), ARM compiler
CMSISM4FPSRC = $(CHIBIOS)/algorithms/CMSIS/arm_cortexM4lf_math.lib

# Required include directories
CMSISINCDIR = $(CHIBIOS)/algorithms/CMSIS/


